<?php
//DEBUG
require_once 'utils/server/server_utils.php';
show_errors();
//CONTROLLA IL LOGIN
require_once 'service/user/user_service.php';
require_once 'utils/user/user_utils.php';
$auth_info = usr_srv_check_login(ADMIN_LVL); ?>

    <!doctype html>
    <html lang="it-it">
    <head>
        <?php include_once 'components/head.php'; ?>
        <title>Registrazione</title>
        <link href="./css/registration.css" rel="stylesheet" media="screen">
    </head>
    <body>
    <?php include_once 'components/navbar.php'; ?>
    <div class="container">
        <div class="py-5 text-center">
            <h2>Form di Registrazione</h2>
            <p class="lead">Per registrarti al sito è necessario che tu inserisca alcuni dati!</p>
        </div>
        <?php registerpg_register(); ?>
        <div class="col-md">
            <form class="needs-validation" novalidate method="post">
                <div class="row">
                    <div class="col-md-6 mb-3">
                        <label for="firstname">First name</label>
                        <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Nome"
                               required>
                        <div class="invalid-feedback">Valid first name is required.</div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="lastname">Last name</label>
                        <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Cognome"
                               required>
                        <div class="invalid-feedback">Valid last name is required.</div>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="you@example.com">
                    <div class="invalid-feedback">Inserire una mail valida</div>
                </div>
                <div class="mb-3">
                    <label for="pass">Password:</label>
                    <ul class="list-group-horizontal">
                        <li class="list-group-item-warning">Almeno una maiuscola</li>
                        <li class="list-group-item-warning mt-1">Almeno una minuscola</li>
                        <li class="list-group-item-warning mt-1">Almeno un numero</li>
                        <li class="list-group-item-warning mt-1">Compresa tra i 7 e i 60 caratteri</li>
                    </ul>
                    <input type="password" class="form-control" id="pass" name="pass" required>
                    <div class="invalid-feedback">Inserire una password valida</div>
                </div>

                <div class="mb-3">
                    <label for="confirm">Conferma password:</label>
                    <input type="password" class="form-control" id="confirm" name="confirm"
                           required>
                    <div class="invalid-feedback">Inserire una password valida</div>
                </div>

                <hr class="mb-4">
                <button class="btn btn-primary btn-lg btn-block" type="submit">Registrati</button>
            </form>
        </div>
    </div>
    <script src="./js/registration.js"></script>
    <script>
        document.getElementById("firstname").value = "<?php if (isset($_POST["firstname"])) echo $_POST["firstname"] ?>";
        document.getElementById("lastname").value = "<?php if (isset($_POST["lastname"])) echo $_POST["lastname"] ?>";
        document.getElementById("email").value = "<?php if (isset($_POST["email"])) echo $_POST["email"] ?>";
    </script>
    </body>
    </html>


<?php
function insert_user($email, $first_name, $last_name, $password, $password_confirm)
{
    $err_msg = "";

    $err_msg = $err_msg.get_name_alert(check_name($first_name),"Nome");
    $err_msg = $err_msg.get_name_alert(check_name($last_name),"Cognome");
    $err_msg = $err_msg.get_email_alert(check_email($email));
    $err_msg = $err_msg.get_password_alert(check_password($password));
    $diff = strcmp($password,$password_confirm);


    if($diff!=0)echo get_password_alert(5); //PASSWORD NON COINCIDONO
    //QUERY AL DB
    echo $err_msg;
    if (empty($err_msg) and $diff==0) {
        $result = usr_srv_register($first_name, $last_name, $email, $password);

        switch ($result) {
            case 0:
                echo get_email_alert(3);
                break;
            case -1:
                echo "<div class=\"alert alert-danger\" role=\"alert\">Errore durante la registrazione</div>";
                break;
            case 1:
                $URL = URL_LOGIN;
                echo "<script type='text/javascript'>document.location.href='{$URL}';</script>";
                break;
            default:
                echo "<div class='errorMessage'>Errore sconosciuto</div>";
        }
    }
}

function registerpg_register()
{
    if (isset($_POST)) {
        if (isset($_POST["firstname"]) and isset($_POST["lastname"]) and isset($_POST["email"]) and isset($_POST["pass"]) and isset($_POST["confirm"])) {
            $first_name = trim($_POST["firstname"]);
            $last_name = trim($_POST["lastname"]);
            $email = strtolower(trim($_POST["email"]));
            $password = trim($_POST["pass"]);
            $password_confirm = trim($_POST["confirm"]);

            insert_user($email, $first_name, $last_name, $password, $password_confirm, null);
        }
    }

}

?>