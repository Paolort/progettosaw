<?php
require_once "mysql_credentials.php";

class Database
{
    // specify your own database credentials
    private $host = DB_HOST;
    private $db_name = DB_NAME;
    private $username = DB_USER;
    private $password = DB_SRV_PWD;
    public $conn;

    /**
     * Permette di ottenere una connessione a un database
     * @return PDO|null la connessione al database, se ci sono errori ritorna null.
     */
    public function getConnection()
    {
        $this->conn = null;

        try {
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name.";charset=utf8", $this->username, $this->password);
        } catch (PDOException $exception) {
            $this->conn = null;
        }

        return $this->conn;
    }
}
