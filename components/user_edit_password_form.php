<link href="/~S4529439/css/components/user_edit_password_form.css" rel="stylesheet" media="screen">
<form method="post" id="passwordForm">
    <div class="card-body">
        <div class="no-for-admin">
        <label id="old_password_label" for="old_password">Current Password</label>
        <div class="form-group pass_show">
            <input id="old_password" name="old_password" type="password" class="form-control" placeholder="Current Password">
        </div>
        </div>
        <label for="new_password">New Password</label>
        <div class="form-group pass_show">
            <input id="new_password" name="new_password" type="password" class="form-control" placeholder="New Password">
        </div>
        <label for="new_password_repeat">Confirm Password</label>
        <div class="form-group pass_show">
            <input id="new_password_repeat" name="new_password_repeat" type="password" class="form-control" placeholder="Confirm Password">
        </div>
    </div>
    <div class="card-footer">
        <input id="btn-change-pwd-submit" type="submit" class="btn btn-primary btn-load btn-lg" value="Cambia Password">
    </div>
</form>
<script src="/~S4529439/js/components/edit_password_form.js"></script>
