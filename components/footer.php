<link rel="stylesheet" href="/~S4529439/css/components/footer.css" media="screen">
<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-6 col-xs-12">
                <p class="copyright-text">Sito sviluppato da Paolo Bono</p>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <ul class="social-icons">
                    <li><a class="mail" href="mailto:4529439@studenti.unige.it"><img alt="Mail" src="/~S4529439/img/svg/contact/gmail.svg"></a></li>
                    <li><a class="linkedin" href="https://www.linkedin.com/in/paolo-bono-2576a3132/"><img alt="Profilo Linkedin" src="/~S4529439/img/svg/contact/linkedin.svg"></a></li>
                    <li><a class="apache" href="https://httpd.apache.org/"><img alt="Apache" src="/~S4529439/img/svg/contact/apache.svg"></a></li>
                    <li><a class="php" href="https://www.php.net/"><img alt="PHP" src="/~S4529439/img/svg/contact/php.svg"></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>