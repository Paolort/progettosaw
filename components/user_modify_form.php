<?php global $message,$personal,$usr_id?>
<div class="container">
    <div class="card">
        <div class="card-header">
            <h1>Profilo utente</h1>
            <h2>Qua puoi visualizzare e modificare le tue informazioni</h2>
            <?php echo $message ?>
        </div>
        <form method="post">
            <div class="card-body">
                <div class="field row">
                    <!--CAMPO FIRST NAME-->
                    <div class="name-field">
                        <label for="firstname">Nome</label>
                        <input type="text" class="form-control disabled-on-visit" name="firstname" id="firstname" placeholder="Il tuo nome">
                    </div>

                    <!--CAMPO LAST NAME-->
                    <div class="name-field">
                        <label for="lastname">Cognome</label>
                        <input type="text" class="form-control disabled-on-visit" name="lastname" id="lastname" placeholder="Il tuo cognome">
                    </div>
                </div>

                <!--CAMPO EMAIL-->
                <div class="field row">
                    <label for="email">E-mail</label>
                    <input type="email" class="form-control disabled-on-visit" name="email" id="email" placeholder="mail@example.it">
                </div>

                <!--PULSANTE MODIFICA PASSWORD-->
                <div class="field row">
                    <a id="edit-pwd-btn" class="btn btn-primary" href="<?php echo !$personal?URL_ADMIN_EDIT_PASSWORD.'?usr_id='.$usr_id:URL_USER_EDIT_PASSWORD?>">
                        Modifica Password</a>
                </div>
                <!--CAMPO DESCRIZIONE-->
                <div class="field row">
                    <label for="description">Descrizione</label>
                    <input type="text" class="form-control disabled-on-visit" name="description" id="description">
                </div>
                <!--CAMPO DATA DI NASCITA-->
                <div class="field row">
                    <label for="bdate">Data di nascita</label>
                    <input type="date" class="form-control disabled-on-visit" name="bdate" id="bdate">
                </div>
            </div>
            <div class="card-footer">
                <input id="sub-btn" type="submit" class="btn btn-primary" value="Salva">
                <a id="modify-link" href="<?php echo URL_UPDATE_PROFILE ?>" class="btn btn-primary">Modifica</a>
            </div>
        </form>
    </div>
</div>