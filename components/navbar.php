<?php global $auth_info; ?>
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="/~S4529439">SAW Forum</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" >
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">

        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo URL_HOME?>">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo URL_PROFILE?>">Profilo</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo URL_CHAT_HOME?>">Chat</a>
            </li>
            <?php
            if ($auth_info->user_auth_lvl == ADMIN_LVL) {
                echo '<li class="nav-item"><a class="nav-link text-primary" href="'.URL_ADMIN_HOME.'">Amministrazione</a></li>';
            }
            ?>
        </ul>



        <div id="search-box" class='form-inline mt-2 mt-md-0'>
            <form action="/~S4529439/site/search_user.php" method="get">
                <input id='search' name='search' type='text' placeholder="Cerca utente">
                <input id='searh-icon' type="submit" class="btn btn-outline-primary mr-sm-2" value="">
            </form>
        </div>

        <a id="search-btn-alternative" class="btn btn-primary mr-sm-2" href="/~S4529439/site/search_user.php">Cerca</a>

        <script src="/~S4529439/js/components/navbar.js"></script>
        <script>let disabled = true;</script> <!--SERVE IN searchShow() vedi sotto-->

        <?php
        if ($auth_info->logged) {
            echo "<script>disabled = false;searchShow();</script>";
            echo "<div class='form-inline mt-2 mt-md-0'><a class='btn btn-outline-secondary mr-sm-2' href='".URL_LOGOUT."'>Logout</a></div>";
        } else {
            echo "<div class='form-inline mt-2 mt-md-0'><a class='btn btn-outline-primary mr-sm-2' href=".URL_LOGIN.">Accedi</a></div>";
            echo "<div class='form-inline mt-2 mt-md-0'><a class='btn btn-outline-primary mr-sm-2' href='".URL_REGISTER."'>Registrati</a></div>";
        }
        ?>

    </div>
</nav>