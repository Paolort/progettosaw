<?php
require_once '../utils/server/server_utils.php';
show_errors();
require_once '../service/user/user_service.php';

$auth_info = usr_srv_check_login(USER_LVL);
if($auth_info->logged)
{
    usr_srv_logout($auth_info->user_id); //SE L'UTENTE È LOGGATO PROCEDO CON LA CANCELLAZIONE TOKEN LATO SERVER
}
else
{
    usr_srv_local_logout();
} // ALTRIMENTI SOLO LOCALMENTE: POTREI CANCELLARE IL TOKEN DI CHIUNQUE ALTRIMENTI
redirect(URL_LOGIN);