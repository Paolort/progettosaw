<?php
require_once '../service/user/user_service.php';
require_once '../utils/server/server_utils.php';
show_errors();
?>
<!doctype html>
<html lang="it-it">
<head>
    <title>Unauthorized</title>
    <link href="../css/unauthorized.css" rel="stylesheet" media="screen">
    <?php include_once '../components/head.php';
    $auth_info = usr_srv_check_login(USER_LVL);
    ?>

</head>
<body>
<main class="container">
    <div class="advice">
        <div class="py-3 text-center">
            <h2>Non Autorizzato</h2>
            <p class="lead">Non hai accesso a queste funzioni!</p>
        </div>
        <div>
            <?php
            if($auth_info->authorized)
            {
                $URL1 = URL_HOME;
                echo '<a class="btn btn-lg btn-primary btn-block my-3" href="'.$URL1.'">Home</a><a class="btn btn-lg btn-secondary btn-block" href="logout.php">Logout</a>';
            }
            else
            {
                $URL2 = URL_LOGIN;
                echo '<a class="btn btn-lg btn-secondary btn-block" href="'.$URL2.'">Login</a>';
            }
            ?>
        </div>

    </div>
</main>
</body>
</html>