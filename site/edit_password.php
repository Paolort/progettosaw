<?php
//DEBUG
require_once '../utils/server/server_utils.php';
show_errors();
//CONTROLLA IL LOGIN
require_once '../service/user/user_service.php';
$auth_info = usr_srv_check_login(USER_LVL);
//REDIRIGE SE NON LOGGATO
if(!$auth_info->authorized) {
    redirect(URL_UNAUTHORIZED);
}
?>

    <!doctype html>
    <html lang="it-it">
    <head>
        <?php include_once '../components/head.php';?>
        <title>Modifica Password</title>
        <link href="../css/site/edit_password.css" rel="stylesheet" media="screen">
    </head>
    <body>
        <?php include_once '../components/navbar.php'; ?>
        <div class="card">
            <div class="card-header">
                <h2>Modifica Password</h2>
                <h3>Puoi modificare la password tramite il form sottostante.</h3>
                <?php echo update_password($auth_info->user_id)?>
            </div>
            <?php include_once '../components/user_edit_password_form.php'?>
        </div>
    </body>

    </html>



<?php
/**
 * Effettua le procedure se i campi nel POST sono impostati, Stampa i messaggi relativi ad avvisi.
 *
 * @param int $id l'utente alla quale modificare la password
 */
function update_password(int $id)
{
    if (isset($_POST["new_password"]) and isset($_POST["new_password_repeat"]) and isset($_POST["old_password"])) {
        $pwd1 = trim($_POST["new_password"]);
        $pwd2 = trim($_POST["new_password_repeat"]);
        $old_password = trim($_POST["old_password"]);
        if (strcmp($pwd1, $pwd2) == 0) { //CHECK DELLE PASSWORD CHE COMBACINO
            $check = check_password($pwd1);
            if ($check != 0) echo get_password_alert($check);
            else {
                $check = usr_srv_edit_my_password($id,$pwd1,$old_password);
                switch ($check) {
                    case -1:
                        echo "<div class=\"alert alert-danger\" role=\"alert\">Errore durante la modifica</div>";
                        break;
                    case -2:
                        echo "<div class=\"alert alert-danger\" role=\"alert\">L'utente non risulta presente</div>";
                        break;
                    case -5:
                        echo "<div class=\"alert alert-danger\" role=\"alert\">Le vecchia password non coincide</div>";
                        break;
                    case 1:
                        echo "<div class=\"alert alert-success\" role=\"alert\">Password modificata con successo</div>";
                        break;
                    default:
                        echo "<div class=\"alert alert-danger\" role=\"alert\">Errore ".$check."</div>";

                }
            }

        } else {
            echo "<div class=\"alert alert-danger\" role=\"alert\">Le password non coincidono</div>";
        }
    }
}

?>