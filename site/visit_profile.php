<?php
//DEBUG
require_once '../utils/server/server_utils.php';
show_errors();
//CONTROLLA IL LOGIN
require_once '../service/user/user_service.php';
$auth_info = usr_srv_check_login(USER_LVL);

//TRUCCO PER FAR SI CHE I TEST FUNZIONINO DATO CHE javascript prende I DATI DOPO IL CARICAMENTO DELLA PAGINA
$data = null;
//REDIRIGE SE NON LOGGATO
if (!$auth_info->authorized) {
    redirect(URL_UNAUTHORIZED);
} else $data = usr_srv_get_personal_data($auth_info->user_id);
?>

<!doctype html>
<html lang="it-it">
<head>
    <?php include_once '../components/head.php'; ?>
    <title>Visita Profilo</title>
    <link href="../css/site/visit_profile.css" rel="stylesheet" media="screen">
</head>
<body>
<?php include_once '../components/navbar.php'; ?>
<div class="container" role="main">
    <div id="card" class="card">
        <div id="color-fill"></div>
        <h1 id="user-name"></h1>
        <p id="user-title" class="title"></p>
        <p id="user-description"></p>
        <p><a class="btn btn-primary" id="send-message">Invia messaggio</a></p>
    </div>
</div>
<span hidden>
    <?php echo $data->firstname . "    " . $data->lastname . "     " . $data->email?>
</span>
</body>
<script src="../js/site/visit_profile.js"></script>
</html>