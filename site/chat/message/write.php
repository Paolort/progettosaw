<?php
CONST PG_WRITE_RECEIVERS_FIELD = "destinatari";
CONST PG_WRITE_TEXT_FIELD = "text";
CONST PG_WRITE_GET_RECEIVERS = "to";


require_once '../../../utils/server/server_utils.php';
show_errors();
require_once '../../../service/user/user_service.php';
require_once '../../../service/msg/msg_service.php';

$auth_info = usr_srv_check_login(USER_LVL);
if(!$auth_info->authorized) {
    redirect(URL_UNAUTHORIZED);
}
?>

<!doctype html>
<html lang="it-it">
<head>
    <?php include_once '../../../components/head.php';?>
    <title>Nuovo Messaggio</title>
    <link href="../../../css/chat/message/write.css" rel="stylesheet" media="screen">
</head>
<body>
<?php require_once '../../../components/navbar.php'; ?>
<div class="container w-100">
    <div class="card mt-3">
        <?php if($auth_info->authorized and isset($_POST["destinatari"]) and isset($_POST["text"])) {
            $result = pg_write_send_message($auth_info->user_id);

            switch ($result) {
                case 0:
                    $to_return = "<div class=\"alert alert-danger mt-2\" role=\"alert\">Uno o più destinatari non esistono</div>";
                    break;
                case -1:
                    $to_return = "<div class=\"alert alert-danger mt-2\" role=\"alert\">Errore durante l'invio dei dati</div>";
                    break;
                case 1:
                    $to_return = "<div class=\"alert alert-success mt-2\" role=\"alert\">Messaggio inviato con successo</div>";
                    break;
                case -2:
                    $to_return = "<div class=\"alert alert-danger mt-2\" role=\"alert\">I Dati non sono stati impostati</div>";
                    break;
                case -3:
                    $to_return = "<div class=\"alert alert-danger mt-2\" role=\"alert\">Errore non gestito</div>";
                    break;
                default:
                    $to_return = "<div class=\"alert alert-danger mt-2\" role=\"alert\">Errore sconosciuto</div>";
            }
            echo $to_return;
        }

        ?>
        <div class="card-header w-100">
            <div class="card-header-tabs clearfix text-center">
                <h1 class="panel-title h1">Nuovo Messaggio</h1>
            </div>
            <div class="card-body">
                <form class="form-horizontal align-content-center text-center" action="write.php" method="POST">
                    <div class="form-group">
                        <label class="" for="destinatari">Destinatari, separati da una virgola</label>
                        <div class="col-sm-8 m-auto">
                            <input type="text" class="form-control text-center" id="destinatari" name="destinatari" placeholder="154,123,653,..."></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-12" for="text">Message</label>
                        <div class="col-sm-12">
                                <textarea class="form-control" id="text" name="text" rows="8" data-gramm="true"></textarea>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary w-25">Invia</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    document.getElementById("destinatari").value = "<?php if (isset($_GET[PG_WRITE_GET_RECEIVERS])) echo $_GET[PG_WRITE_GET_RECEIVERS] ?>";
</script>

</body>

</html>

<?php
function pg_write_send_message(int $id_mittente) : int
{
    if(isset($_POST[PG_WRITE_RECEIVERS_FIELD]) and isset($_POST[PG_WRITE_TEXT_FIELD]))
    {
        $proto = mb_split(",",$_POST["destinatari"]);
        $count = count($proto);

        //Rimozione spazi e tabulazioni
        for($i = 0; $i < $count; $i++)
        {
            $proto[$i] = trim($proto[$i]);
        }
        return msg_srv_send_message($id_mittente,$_POST[PG_WRITE_TEXT_FIELD],$proto);
    }
    return -2;
}
?>