<?php
//DEBUG
require_once '../../utils/server/server_utils.php';
show_errors();
//CONTROLLA IL LOGIN
require_once '../../service/user/user_service.php';
$auth_info = usr_srv_check_login(USER_LVL);
//REDIRIGE SE NON LOGGATO
if (!$auth_info->authorized) {
    redirect(URL_UNAUTHORIZED);
}
?>

<!doctype html>
<html lang="it-it">
<head>
    <?php include_once '../../components/head.php'; ?>
    <title>Chat</title>
    <link href="../../css/chat/chat.css" rel="stylesheet" id="chat-css">
</head>
<body>
<?php include_once '../../components/navbar.php'; ?>

<main id="main" >
    <div id="sidebar" class="sidebar">
        <div class="closebtn" onclick="closeNav()">&times;</div>
        <div id="get_unread" class="border rounded-pill">
            <img alt="" src="../../img/svg/icons/message/inbox-arrow-down.svg">Messaggi da leggere
        </div>
        <div id="get_received" class="border rounded-pill">
            <img alt="" src="../../img/svg/icons/message/inbox-full.svg" style="fill: white">Messaggi ricevuti
        </div>
        <div id="get_sent" class="border rounded-pill">
            <img alt="" src="../../img/svg/icons/message/inbox-arrow-up.svg">Messaggi inviati
        </div>
    </div>

    <div id="main-chat">
        <button class="openbtn" id="openbtn">&#9776; Open Sidebar</button><a class="btn btn-primary new-message-btn" href="<?php echo URL_MESSAGE_WRITE?>">Nuovo Messaggio</a>
        <div class="tab-content ">
            <div class="tab-pane fade show active" id="content-box" role="tabpanel">

                <div id="errorMessage" class="alert alert-danger" role="alert"></div>

                <h1 id="dinamic-title" class="h1"></h1>

                <!--PER LA VISUALIZZAZIONE A ELENCO-->
                <table id="message-table" class="table">

                </table>

                <!--PER LA VISUALIZZAZIONE DEL MESSAGGIO SINGOLO-->
                <div id="view-message" class="content_box">
                    <h2 id="sender" class="h2 content_box p-3 text-center"></h2>
                    <div id="message-text" class="content_box border text-left p-2 border-dark rounded"></div>
                    <a id="message-link" class="btn btn-primary text-right float-right mr-2 mt-2"
                       href="./message/write.php">Rispondi</a>
                </div>


            </div>
            <div id="page-selector" class="card-footer">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li id="previous" class="page-item"><a class="page-link">Indietro</a></li>
                        <li id="element-before" class="page-item"><a class="page-link">1</a></li>
                        <li id="element-actual" class="page-item active"><a class="page-link">2</a></li>
                        <li id="element-next" class="page-item"><a class="page-link">3</a></li>
                        <li id="next" class="page-item"><a class="page-link">Avanti</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <script src="../../js/chat/chat.js"></script>
</main>
</body>
</html>