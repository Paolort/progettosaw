<?php
//DEBUG
require_once '../utils/server/server_utils.php';
show_errors();
//CONTROLLA IL LOGIN
require_once '../service/user/user_service.php';
$auth_info = usr_srv_check_login(USER_LVL);
//REDIRIGE SE NON LOGGATO
if(!$auth_info->authorized) {
    redirect(URL_UNAUTHORIZED);
}
else $personalData = usr_srv_get_personal_data($auth_info->user_id);

?>

<!doctype html>
<html lang="it-it">
<head>
    <?php include_once '../components/head.php';?>
    <title>Home</title>
    <link href="../css/site/home.css" rel="stylesheet" media="screen">
</head>
<body>
<?php include_once '../components/navbar.php'; ?>

<main class="container">
    <div id="welcome-message" class="jumbotron">
        <h1>Benvenuto nella tua home, <?php echo $personalData->firstname . " " . $personalData->lastname; ?></h1>
        <p class="lead">Per adesso non abbiamo nulla da proporti, riprova più tardi</p>
        <a id="logoutBut" class="btn btn-lg btn-primary" role="button" href="<?php echo URL_LOGOUT?>">Logout</a>
        <a id="viewUser" class="btn btn-lg btn-primary" role="button" href="<?php echo URL_ADMIN_USER_LIST?>">Lista Utenti</a>
    </div>
</main>
</body>

</html>
