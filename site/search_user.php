<?php
//DEBUG
require_once '../utils/server/server_utils.php';
show_errors();
//CONTROLLA IL LOGIN
require_once '../service/user/user_service.php';
$auth_info = usr_srv_check_login(USER_LVL);
//REDIRIGE SE NON LOGGATO
if (!$auth_info->authorized) {
    redirect(URL_UNAUTHORIZED);
}
?>

<!doctype html>
<html lang="it-it">
<head>
    <?php include_once '../components/head.php'; ?>
    <title>Cerca Utenti</title>
    <link href="../css/site/search_user.css" rel="stylesheet" media="screen">
</head>
<body>
<?php include_once '../components/navbar.php'; ?>
<main class="container">
    <div class="card">
        <div class="card-header">
            <form method="get">
                <label for="search">Parametri: </label>
                <input type="text" name="search" id="search_below">
                <input type="submit" value="Cerca" class="btn btn-primary">
            </form>
        </div>
        <div class="card-body">
            <h2 class="mt-3">Risultati della ricerca:</h2>
            <div id="user-list" class="list-group"></div>
        </div>
    </div>
</main>
<script src="../js/site/search_user.js"></script>
</body>

</html>