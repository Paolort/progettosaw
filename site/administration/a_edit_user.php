<?php
//DEBUG
require_once '../../utils/server/server_utils.php';
show_errors();
//CONTROLLA IL LOGIN
require_once '../../service/user/user_service.php';
$auth_info = usr_srv_check_login(ADMIN_LVL);
//REDIRIGE SE NON LOGGATO
if (!$auth_info->authorized) {
    redirect(URL_UNAUTHORIZED);
}
global $message, $usr_id, $personal; //SERVONO A user_modify_form.php
$personal = false; // SERVE PER COLLEGAMENTO CAMBIO PASSWORD

?>

<!doctype html>
<html lang="it-it">
<head>
    <?php include_once '../../components/head.php'; ?>
    <title>Profilo</title>
    <link href="../../css/update_profile.css" rel="stylesheet" media="screen">
</head>
<body>
<?php include_once '../../components/navbar.php';
$message = main();// MESSAGGIO PER IL FORM IMPORTATO
include_once '../../components/user_modify_form.php'
?>

<script src="../../js/site/administration/a_edit_user.js"></script>
<script type="text/javascript">
    document.getElementById("firstname").value = "<?php if (isset($user->firstname))echo $user->firstname; ?>";
    document.getElementById("lastname").value = "<?php if (isset($user->lastname)) echo $user->lastname; ?>";
    document.getElementById("email").value = "<?php if (isset($user->email)) echo $user->email; ?>";
    document.getElementById("bdate").value = "<?php if (isset($user->b_date)) echo $user->b_date;?>";
    document.getElementById("description").value = "<?php if (isset($user->description)) echo $user->description;?>";
</script>
</body>
</html>

<?php

/**
 * Permette di modificare il profilo. Richiede che sia impostato nella $GET il campo usr_id contenente l'id della persona da modificare
 *
 * @return string
 */
function main() : string
{
    global $user,$usr_id;
    if(isset($_GET["usr_id"]) and is_numeric($_GET["usr_id"]))
    {
        $user = get_user($_GET["usr_id"]);
        $usr_id = $user->id;
        if($user!=null) {
            if (isset($_POST["firstname"]) and isset($_POST["lastname"]) and isset($_POST["email"])) {
                $message = update_all($user->id);
                $user = get_user($_GET["usr_id"]); //RIAGGIORNA I DATI IN CASO DI MODIFICA
                return $message;
            } else return "";
        } else return '<div class="alert alert-danger" role="alert">L\'UTENTE RICHIESTO NON È STATO TROVATO</div>';
    }
    else return "<div class=\"alert alert-danger\" role=\"alert\">MANCA IL PARAMETRO usr_id NELLA RICHIESTA. Non sarà possibile effettuare modifiche</div>";
}


/**
 * Permette di ottenere l'utente della quale si vuole modificare la password
 */
function get_user(int $id) : User
{
    return usr_srv_a_get_user_data($id);
}

/**
 * Permette di cambiare i dati dell'utente
 * @param int $id l'id dell'utente alla quale cambiare i dati
 * @return string l'eventuale messaggio da stampare
 */
function update_all(int $id) : string
{
    $firstname = trim($_POST["firstname"]);
    $lastname = trim($_POST["lastname"]);
    $email = strtolower(trim($_POST["email"]));
    $bdate = trim($_POST["bdate"]);
    $description = isset($_POST["description"]) ? $_POST["description"] : "";

    $err_msg = check_data_get_message_no_pwd($firstname,$lastname,$email,$bdate);


    if(empty($err_msg))
    {
        $edit_result = usr_srv_update_profile_all($id,$firstname, $lastname, $email, $bdate, $description);
        return print_message($edit_result);
    } else return $err_msg;
}

/**
 * Ritorna il messaggio di errore da stampare dato il codice di errore
 * @param $edit_result
 * @return string
 */
function print_message($edit_result)  : string
{
    switch ($edit_result) {
        case 0:
            $to_return = get_email_alert(3);
            break;
        case -1:
            $to_return = "<div class=\"alert alert-danger\" role=\"alert\">Non consentito</div>";
            break;
        case 1:
            $to_return = "<div class=\"alert alert-success\" role=\"alert\">Modifica effettuata con successo</div>";
            break;
        case -4:
            $to_return = "<div class=\"alert alert-danger\" role=\"alert\">I dati non sono variati</div>";
            break;
        case -5:
            $to_return = "<div class=\"alert alert-danger\" role=\"alert\">Errore exec</div>";
            break;
        default:
            $to_return = "<div class=\"alert alert-danger\" role=\"alert\">Errore sconosciuto: ".$edit_result."</div>";
    }
    return $to_return;
}
?>

