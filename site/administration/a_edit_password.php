<?php
//DEBUG
require_once '../../utils/server/server_utils.php';
show_errors();
//CONTROLLA IL LOGIN
require_once '../../service/user/user_service.php';
$auth_info = usr_srv_check_login(ADMIN_LVL);
//REDIRIGE SE NON LOGGATO
if(!$auth_info->authorized) {
    redirect(URL_UNAUTHORIZED);
}
?>

    <!doctype html>
    <html lang="it-it">
    <head>
        <?php include_once '../../components/head.php';?>
        <title>Amministratore: Modifica Password</title>
        <link href="/~S4529439/css/site/administration/a_edit_password.css" rel="stylesheet" media="screen">
        <script src="/~S4529439/js/site/administration/a_edit_password.js"></script>
    </head>
    <body>
    <?php include_once '../../components/navbar.php'; ?>
    <div class="card">
        <div class="card-header">
            <h2>Amministrazione: Modifica Password</h2>
            <h3>Puoi modificare la password dell'utente selezionato tramite questo form.</h3>
            <?php echo main()?>
        </div>
        <?php include_once '../../components/user_edit_password_form.php'?>
    </div>
    </body>
</html>




<?php

function main()
{
    if(isset($_GET["usr_id"]) and is_numeric($_GET["usr_id"]))
    {
        $user = get_user($_GET["usr_id"]);
        if($user!=null)
        {
            echo '<div class="alert alert-info" role="alert">Stai modificando la password di -> ID:'.$user->id.' - Nome:'.$user->firstname.' - Cognome:'.$user->lastname.'</div>';
            update_password($user->id);
        }
        else
        {
            echo '<div class="alert alert-danger" role="alert">L\'UTENTE RICHIESTO NON È STATO TROVATO</div>';
        }
    } else echo '<div class="alert alert-danger" role="alert">MANCA IL PARAMETRO usr_id NON SARA MODIFICATO ALCUN DATO</div>';
}


/**
 * Permette di ottenere l'utente della quale si vuole modificare la password
 */
function get_user(int $id) : User
{
    return usr_srv_a_get_user_data($id);
}




/**
 * Effettua le procedure se i campi nel POST sono impostati, ritorna feedback per l'utente da stampare
 *
 * @param int $id l'utente alla quale modificare la password
 * @return int
 */
function update_password(int $id)
{
    if (isset($_POST["new_password"]) and isset($_POST["new_password_repeat"])) {
        $pwd1 = trim($_POST["new_password"]);
        $pwd2 = trim($_POST["new_password_repeat"]);
        if (strcmp($pwd1, $pwd2) == 0) { //CHECK DELLE PASSWORD CHE COMBACINO
            $check = check_password($pwd1);
            if ($check != 0) echo get_password_alert($check);
            else {
                $check = usr_srv_a_edit_password($id,$pwd1,"",false);
                switch ($check) {
                    case -1:
                        echo "<div class=\"alert alert-danger\" role=\"alert\">Errore durante la modifica</div>";
                        break;
                    case -2:
                        echo "<div class=\"alert alert-danger\" role=\"alert\">L'utente non risulta presente</div>";
                        break;
                    case 1:
                        echo "<div class=\"alert alert-success\" role=\"alert\">Password modificata con successo</div>";
                        break;
                    default:
                        echo "<div class=\"alert alert-danger\" role=\"alert\">Errore ".$check."</div>";

                }
            }

        } else {
            echo "<div class=\"alert alert-danger\" role=\"alert\">Le password non coincidono</div>";
        }
    }
}

?>