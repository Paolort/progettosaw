<?php
require_once '../../utils/server/server_utils.php';
show_errors();
require_once "../../service/user/user_service.php";
?>

<!doctype html>
<html lang="it-it">
<head>
    <?php include_once '../../components/head.php';?>
    <title>Amministrazione</title>
</head>
<body>
<?php
$personalData = null;
$auth_info = usr_srv_check_login(ADMIN_LVL);
include "../../components/navbar.php";
if(!$auth_info->authorized) {
    redirect(URL_UNAUTHORIZED);
} else $personalData = usr_srv_get_personal_data($auth_info->user_id);

?>
<main class="container">
    <div class="jumbotron">
        <h1>Benvenuto nella pagina di amministrazione, <?php echo $personalData->firstname . " " . $personalData->lastname?></h1>
        <p class="lead">Puoi vedere le azioni disponibili dal menù sottostante</p>
    </div>
    <div class="album py-5 bg-light">
        <div class="container">

            <div class="row">
                <div class="col-md-4">
                    <div class="card mb-4 shadow-sm">
                        <div class="card-body">
                            <p class="card-title font-weight-bold">Gestione utenti</p>
                            <p class="card-text">Visualizza l'elenco utenti e gestiscili singolarmente</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <a class="btn btn-md btn-outline-primary" href="<?php echo URL_ADMIN_USER_LIST?>">Gestione utenti</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
</body>