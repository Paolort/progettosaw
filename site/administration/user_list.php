<?php
//DEBUG
require_once '../../utils/server/server_utils.php';
show_errors();
//CONTROLLA IL LOGIN
require_once '../../service/user/user_service.php';
$auth_info = usr_srv_check_login(ADMIN_LVL);
//REDIRIGE SE NON LOGGATO
if(!$auth_info->authorized) {
    redirect(URL_UNAUTHORIZED);
}
?>

<!doctype html>
<html lang="it-it">
<head>
    <?php include_once '../../components/head.php';?>
    <title>Elenco Utenti</title>
    <link href="../../css/administration/user_list.css" rel="stylesheet" media="screen">
</head>
<body>
<?php include_once '../../components/navbar.php'; ?>
<div class="container">
    <h1>Elenco utenti</h1>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Nome</th>
            <th scope="col">Cognome</th>
            <th scope="col">Email</th>
            <th scope="col">Verified</th>
            <th scope="col">Auth Level</th>
            <th scope="col">Data Nascita</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <?php
        if($auth_info->authorized) {
            $result = usr_srv_a_user_list(0);
            if (isset($result)) {
                foreach ($result as $user) {
                    echo $user->toHtmlGridElement();
                }
            }
        }
        ?>
        </tbody>
    </table>

</div>
</body>
</html>