<?php
//DEBUG
require_once './utils/server/server_utils.php';
show_errors();
//CONTROLLA IL LOGIN
require_once './service/user/user_service.php';
$auth_info = usr_srv_check_login(ADMIN_LVL);
//REDIRIGE SE NON LOGGATO
if(!$auth_info->authorized) {
    redirect(URL_UNAUTHORIZED);
}
?>

<!doctype html>
<html lang="it-it">
<head>
    <?php include_once './components/head.php';?>
    <title>Titolo</title>
    <link href="css/" rel="stylesheet" media="screen">
</head>
<body>
<?php include_once './components/navbar.php'; ?>
</body>

</html>