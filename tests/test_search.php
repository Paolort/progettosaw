<?php

function search($first_name, $last_name, $baseurl = 'http://localhost')
{

    $search = urlencode($first_name ." ". $last_name);

    $ch = curl_init();

    $url = $baseurl."/api/user/search_user.php";

    $cookieFile = "cookies";
    if (!file_exists($cookieFile)) {
        $fh = fopen($cookieFile, "w");
        fwrite($fh, "");
        fclose($fh);
    }

    curl_setopt($ch, CURLOPT_URL, $url . "?data=".$search);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $headers = array();
    $headers[] = 'Content-Type: application/x-www-form-urlencoded';
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFile); // Cookie aware
    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieFile); // Cookie aware
    $result = curl_exec($ch);

    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);

    return $result;
}

function check_correct_search($first_name, $last_name, $show_page)
{
    return strpos($show_page, $first_name) && strpos($show_page, $last_name);
}
