<?php
//DEBUG
require_once 'utils/server/server_utils.php';
show_errors();
//CONTROLLA IL LOGIN
require_once 'service/user/user_service.php';
$auth_info = usr_srv_check_login(USER_LVL);
//REDIRIGE SE NON LOGGATO
if (!$auth_info->authorized) {
    redirect(URL_UNAUTHORIZED);
}

global $profile_data, $personal;
$personal = true; // SERVE PER IL FORM IMPORTATO
$profile_data = usr_srv_get_personal_data($auth_info->user_id);

?>

<!doctype html>
<html lang="it-it">
<head>
    <?php include_once 'components/head.php'; ?>
    <title>Visualizzazione Profilo</title>
    <link href="./css/show_profile.css" rel="stylesheet" media="screen">
</head>
<body>
<?php include_once 'components/navbar.php';
include_once 'components/user_modify_form.php'
?>
<span hidden>
    <?php echo $profile_data->firstname . "    " . $profile_data->lastname . "     " . $profile_data->email?>
</span>
<script src="./js/show_profile.js"></script>
<script>
    document.getElementById("firstname").value = "<?php if (isset($profile_data->firstname)) echo $profile_data->firstname ?>";
    document.getElementById("lastname").value = "<?php if (isset($profile_data->lastname)) echo $profile_data->lastname ?>";
    document.getElementById("email").value = "<?php if (isset($profile_data->email)) echo $profile_data->email ?>";
    document.getElementById("bdate").value = "<?php if (isset($profile_data->b_date)) echo $profile_data->b_date?>";
    document.getElementById("description").value = "<?php if (isset($profile_data->description)) echo $profile_data->description?>";
</script>
</body>
</html>
