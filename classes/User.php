<?php
const DB_USR_ID = "usr_id";
const DB_USR_FIRSTNAME = "usr_firstname";
const DB_USR_LASTNAME = "usr_lastname";
const DB_USR_EMAIL = "usr_email";
const DB_USR_MAIL_VER = "usr_mail_verified";
const DB_USR_AUTH_LVL = "usr_auth_lvl";
const DB_USR_PWD = "usr_pwd";
const DB_USR_DESCRIPTION = "usr_description";
const DB_USR_B_DATE = "usr_b_date";
const DB_USR_TOKEN = "usr_token";
const DB_USR_EXPIRES = "usr_expires";

const ADMIN_LVL = 10;
const USER_LVL = 0;

class User
{
    public $id;
    public $firstname;
    public $lastname;
    public $email;
    public $mail_verified;
    public $auth_lvl;
    public $pwd;
    public $description;
    public $b_date;
    public $token;
    public $expires;

    function __construct()
    {
        $a = func_get_args();
        $i = func_num_args();
        if (method_exists($this, $f = '__construct' . $i)) {
            call_user_func_array(array($this, $f), $a);
        }
    }

    function __construct1($row)
    {
        if (isset($row[DB_USR_ID])) $this->id = $row[DB_USR_ID];
        if (isset($row[DB_USR_FIRSTNAME])) $this->firstname = $row[DB_USR_FIRSTNAME];
        if (isset($row[DB_USR_LASTNAME])) $this->lastname = $row[DB_USR_LASTNAME];
        if (isset($row[DB_USR_EMAIL])) $this->email = $row[DB_USR_EMAIL];
        if (isset($row[DB_USR_MAIL_VER])) $this->mail_verified = $row[DB_USR_MAIL_VER];
        if (isset($row[DB_USR_AUTH_LVL])) $this->auth_lvl = $row[DB_USR_AUTH_LVL];
        if (isset($row[DB_USR_PWD])) $this->pwd = $row[DB_USR_PWD];
        if (isset($row[DB_USR_DESCRIPTION])) $this->description = $row[DB_USR_DESCRIPTION];
        if (isset($row[DB_USR_B_DATE])) $this->b_date = $row[DB_USR_B_DATE];
        if (isset($row[DB_USR_TOKEN])) $this->token = $row[DB_USR_TOKEN];
        if (isset($row[DB_USR_EXPIRES])) $this->expires = $row[DB_USR_EXPIRES];
    }

    function toHtmlGridElement()
    {
        $to_return = "";
        $to_return = $to_return . "<tr><th scope=\"row\">".$this->id."</th>";
        $to_return = $to_return . "<td>".(string)$this->firstname."</td>";
        $to_return = $to_return . "<td>".(string)$this->lastname."</td>";
        $to_return = $to_return .  "<td>".(string)$this->email."</td>";
        $to_return = $to_return .  "<td>".(string)$this->mail_verified."</td>";
        $to_return = $to_return . "<td>".(string)$this->auth_lvl."</td>";
        //$to_return = $to_return .  "<td>".(string)$this->description."</td>";
        $to_return = $to_return .  "<td>".(string)$this->b_date."</td>";
        $to_return = $to_return .  '<td><a class="btn btn-primary" href="/~S4529439/site/administration/a_edit_user.php?usr_id=' . (string)$this->id . '">Profilo</a></td></tr>';
        return $to_return;
    }


}

