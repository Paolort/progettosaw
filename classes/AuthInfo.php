<?php


class AuthInfo
{
    /**
     * @var bool true se autorzzato false altrimenti
     */
    public $authorized;
    /**
     * @var bool indica se l'utente è loggato
     */
    public $logged;
    /**
     * @var int indica l'id dell'utente
     */
    public $user_id;
    /**
     * @var int valore tra 1 e 10 dove indica il grado di autorizzarione vedere User.php per i dettagli sul livello
     */
    public $user_auth_lvl;

    function __construct()
    {
        $this->authorized = false;
        $this->logged = false;
        $this->user_id = null;
        $this->user_auth_lvl= null;
    }
}