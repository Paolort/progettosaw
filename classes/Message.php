<?php

CONST DB_MSG_TABLENAME = "message";
CONST DB_MSG_ID = "msg_id";
CONST DB_MSG_TEXT = "msg_text";
CONST DB_MSG_PRIOR = "msg_priority";
CONST DB_MSG_SENDER = "msg_sender";
CONST DB_MSG_TIME = "msg_time";
CONST DB_MSG_DEL = "msg_deleted_by_sender";

class Message
{
    public $msg_id;
    public $msg_text;
    public $msg_priority;
    public $msg_sender;
    public $msg_time;
    public $msg_deleted_by_sender;

    function __construct()
    {
        $a = func_get_args();
        $i = func_num_args();
        if (method_exists($this, $f = '__construct' . $i)) {
            call_user_func_array(array($this, $f), $a);
        }
    }

    function __construct1($row)
    {
        if (isset($row[DB_MSG_ID])) $this->msg_id = $row[DB_MSG_ID];
        if (isset($row[DB_MSG_TEXT])) $this->msg_text = $row[DB_MSG_TEXT];
        if (isset($row[DB_MSG_PRIOR])) $this->msg_priority = $row[DB_MSG_PRIOR];
        if (isset($row[DB_MSG_SENDER])) $this->msg_sender = $row[DB_MSG_SENDER];
        if (isset($row[DB_MSG_TIME])) $this->msg_time = $row[DB_MSG_TIME];
        if (isset($row[DB_MSG_DEL])) $this->msg_deleted_by_sender = $row[DB_MSG_DEL];
    }
}