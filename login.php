<?php
require_once 'utils/server/server_utils.php';
show_errors();

require_once "service/user/user_service.php";
global $auth_info;
$auth_info = usr_srv_check_login(USER_LVL);
?>


<!doctype html>
<html lang="it-it">
<head>
    <?php include_once 'components/head.php';?>
    <title>Login</title>
    <link href="./css/login.css" rel="stylesheet" media="screen">
</head>
<body>
<?php
include "components/navbar.php";
?>

<div class="card">
<form class="form-signin" method="POST" action="login.php">
    <h1 class="h3 mb-3 font-weight-normal">Effettua l'accesso</h1>
    <label for="email" class="sr-only">Email address</label>
    <input name="email" type="email" id="email" class="form-control" placeholder="Email address" required
           autofocus>
    <label for="pass" class="sr-only">Password</label>
    <input name="pass" type="password" id="pass" class="form-control" placeholder="Password" required>

    <?php
    if (isset($_POST)) {
        if (isset($_POST["email"]) and isset($_POST["pass"])) {
            $email = trim($_POST["email"]);
            $pwd = trim($_POST["pass"]);
            $authenticated = false;
            $authenticated = usr_srv_login($email, $pwd);
            if ($authenticated == true) redirect(URL_HOME);
            else
            {
                echo "<div class=\"alert alert-warning\" role=\"alert\">Credenziali non valide</div>";
            }
        }
    }

    if($auth_info->authorized)redirect(URL_HOME);
    ?>

    <input type="submit" class="btn btn-lg btn-primary btn-block my-2 my-sm-0" value="Accedi">
    <br>
    Crea un'account
    <a class="btn btn-lg btn-secondary btn-block" href="<?php echo URL_REGISTER?>">Registrati</a>
</form>
</div>
</body>
</html>


<?php
function login($email, $pass, $db_connection) {
    try {
        usr_srv_login($email, $pass);
    } catch (Exception $e) {
        echo "<div class=\"alert alert-warning\" role=\"alert\">Errore durante generazione token, riprova</div>";
    }
}
?>