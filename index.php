<?php
//DEBUG
require_once 'utils/server/server_utils.php';
show_errors();
//CONTROLLA IL LOGIN
require_once 'service/user/user_service.php';
$auth_info = usr_srv_check_login(USER_LVL);

?>

<!doctype html>
<html lang="it-it">
<head>
    <?php include_once 'components/head.php'; ?>
    <title>Saw Forum</title>
    <link href="./css/index.css" rel="stylesheet" media="screen">
</head>
<body>
<?php include_once 'components/navbar.php'; ?>

<header id="testa" class="masthead text-white text-center">
    <div class="overlay"></div>
    <div>
        <div class="row">
            <div id="welcome-message" class="col-xl-9">
                <h1>Benvenuto sul forum di SAW!</h1>
                <h2>Registrati e potrai usufruire delle funzioni!</h2>
            </div>
        </div>
        <div class="row">
            <div id="registrer-button-container" class="col-12 col-md-3">
                <a href="<?php echo URL_REGISTER?>" class="btn btn-block btn-lg btn-outline-light">Registrati!</a>
            </div>
        </div>
    </div>
</header>

<!-- Icons Grid -->
<section id="sezione-1" class="features-icons bg-light text-center">
    <div class="container">
        <div class="row pb-2">
            <div class="col-lg-4">
                <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                    <div class="features-icons-icon d-flex">
                        <i class="icon-screen-desktop m-auto text-primary"></i>
                    </div>
                    <h3>Chat Integrata</h3>
                    <p class="lead mb-0">Scambiati opinioni, consigli e richieste di aiuto tramite la chat
                        integrata!</p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                    <div class="features-icons-icon d-flex">
                        <i class="icon-layers m-auto text-primary"></i>
                    </div>
                    <h3>Ricerca Utenti</h3>
                    <p class="lead mb-0">Cerca i tuoi amici, o compagni di corso!</p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="features-icons-item mx-auto mb-0 mb-lg-3">
                    <div class="features-icons-icon d-flex">
                        <i class="icon-check m-auto text-primary"></i>
                    </div>
                    <h3>Funzionalità in sviluppo</h3>
                    <div class="lead mb-0"><ul><li>Post(forum)</li></ul></div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Image Showcases -->
<section class="showcase">
    <div class="container-fluid p-0">
        <div class="row">

            <div id="chat-img" class="col-lg-6 order-lg-2 text-white"></div>
            <div class="col-lg-6 order-lg-1 my-auto text-showcase">
                <h2>Chat integrata!</h2>
                <p class="lead mb-0">Utilizza la chat integrata per comunicare con gli altri membri del forum</p>
            </div>
        </div>
        <div class="row">
            <div id="searh-img" class="col-lg-6 text-white "></div>
            <div class="col-lg-6 my-auto text-showcase">
                <h2>Cerca i tuoi amici</h2>
                <p class="lead mb-0">Puoi cercare i tuoi amici tramite l'apposita barra!</p>
            </div>
        </div>
        <div class="row">
            <div id="responsive-img" class="col-lg-6 order-lg-2 text-white showcase-img"></div>
            <div class="col-lg-6 my-auto text-showcase">
                <h2>Responsive design</h2>
                <p class="lead mb-0">Il sito è stato ottimizzato il più possibile per gli ambienti mobile!</p>
            </div>
        </div>
    </div>
</section>

<?php include_once 'components/footer.php'?>
</body>

</html>