$(document).ready(function () {
    let searchParams = new URLSearchParams(window.location.search);
    if(searchParams.has('search')) {
        let param = searchParams.get('search');
        searchUsers(param)
    }
    else
    {
        $('#card').html("<div class=\"alert alert-warning mt-3\" role=\"alert\">Parametri di ricerca non impostati</div>");
    }
});

function searchUsers(searchParams)
{
    if(searchParams.length>0) {
        $.ajax({
            url: '/~S4529439/api/user/search_user.php?data=' + searchParams,
            dataType: "json",
            data: users,
            success: function (data) {
                let result = "";
                $.each(data, function (i, item) {
                    result += '<a class="list-group-item" href="/~S4529439/site/visit_profile.php?id=' + item.id + '">' + item.id + ' ' + item.firstname + ' ' + item.lastname + '</a>'
                });


                if (result.length <= 0) {
                    $('#user-list').html("<div class=\"alert alert-warning mt-3\" role=\"alert\">Non è stato possibile trovare gli utenti corrispondenti ai" +
                        " parametri di ricerca</div>");
                } else
                    $('#user-list').html(result);
            },
            error: function (xhr) {
                if(xhr.status==404)
                {
                    $('#user-list').html("<div class=\"alert alert-warning mt-3\" role=\"alert\">Non è stato possibile trovare gli utenti corrispondenti ai" +
                        " parametri di ricerca</div>");
                }
            }
        });
    }
    else
    {
        $('#user-list').html("<div class=\"alert alert-warning mt-3\" role=\"alert\">I parametri di ricerca sono vuoti!</div>");
    }
}

