$(document).ready(function () {
    let searchParams = new URLSearchParams(window.location.search);
    if(searchParams.has('id')) {
        let param = searchParams.get('id');
        getUser(param);
    }
    else
    {
        $('#user-name').html("<div class=\"alert alert-warning mt-3\" role=\"alert\">Id dell'utente non specificato</div>");
        $('#send-message').hide();
    }
    }
);


function hashCode(str) { // java String#hashCode
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
}

function intToRGB(i){
    var c = (i & 0x00FFFFFF)
        .toString(16)
        .toUpperCase();

    return "00000".substring(0, 6 - c.length) + c;
}

function getUser(id) {
    let message = new Object();
    $.ajax({
        url: '/~S4529439/api/user/get_user_info.php?id=' + id,
        type: 'GET',
        dataType: 'JSON',
        data: message,
        success: function (data) {
            setUserData(data);
            setHashColor(data.id);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr)
        }
    });
}

function setUserData(data) {
    $('#user-name').text(data.firstname + " " + data.lastname);
    $('#user-description').text(data.description);
    $('#send-message').prop("href",'/~S4529439/site/chat/message/write.php?to=' + data.id);
    let lvl = data.auth_lvl;
    if(lvl=="10")$('#user-title').text("Amministratore");
    else $('#user-title').text("Utente Base");
}

function setHashColor(id) {
    let hash = hashCode(id);
    let color = intToRGB(hash);
    $('#color-fill').css("background-color","#"+color)
}