let status = 0;
let page = 0;

/**
 * Quando la pagina è caricata mostra i messaggi e inizializza i valori di default
 */
$(document).ready(function () {
    getAndShowUnreadReceivedMessages();
    initializePage();
});

/**
 * Quando si clicca si messaggi non letti
 */
$("#get_unread").click(function () {
    getAndShowUnreadReceivedMessages();
});

/**
 * Click su messaggi ricevuti
 */
$("#get_received").click(function () {
    getAndShowAllReceivedMessages();
});

/**
 * Click su messaggi inviati
 */
$("#get_sent").click(function () {
    getAndShowSentMessages();
});

/**
 * Click sul pulsante principale della chat
 */
$("#openbtn").click(function () {
    if($('#sidebar').width()!=0)closeNav();
    else openNav();
});

/**
 * Click su indietro di 2 pagine
 */
$('#element-before').click(function () {
    changePage(-2)
});

/**
 * Click su aventi di 2 pagine
 */
$('#element-next').click(function () {
    changePage(+2);
});

$('#next').click(function () {
    changePage(+1);
});

$('#previous').click(function () {
    changePage(-1);
});

/**
 * Prepara la barra delle pagine
 */
function initializePage() {
    $('#previous').hide();
    $('#element-before').hide();
    $('#element-actual').find("a").text(page+1);
    $('#element-next').find("a").text(page+3);
}

function changePage(increment) {
    if(page+increment>=0)
    {
        page = page+increment;

        switch (page) {
            case 0:
                $('#previous').hide();
                $('#element-before').hide();
                break;
            case 1:
                $('#previous').show();
                $('#element-before').hide();
                break;
            default:
                $('#previous').show();
                $('#element-before').show();
        }
        $('#element-before').find("a").text(page-1);
        $('#element-actual').find("a").text(page+1);
        $('#element-next').find("a").text(page+3);

        refreshMessages();
    }
}

function refreshMessages() {
    switch (status) {
        case 0:
            getAndShowUnreadReceivedMessages();
            break;
        case 1:
            getAndShowAllReceivedMessages();
            break;
        case 2:
            getAndShowSentMessages();
            break;
        default:

    }
}

function getAndShowUnreadReceivedMessages() {
    status = 0;
    callMessageApi('/~S4529439/api/message/get_unread_messages.php?page='+page);
    $('#dinamic-title').html("Messaggi da leggere");
    $('#openbtn').text("☰ Messaggi da leggere");
    $("#get_unread").css("background-color", "#676C72");
    $("#get_received").css("background-color", "#343a40");
    $("#get_sent").css("background-color", "#343a40");
}

function getAndShowAllReceivedMessages() {
    status = 1;
    callMessageApi('/~S4529439/api/message/get_received_messages.php?page='+page);
    $('#dinamic-title').html("Messaggi Ricevuti");
    $('#openbtn').text("☰ Messaggi Ricevuti");
    $("#get_unread").css("background-color", "#343a40");
    $("#get_received").css("background-color", "#676C72");
    $("#get_sent").css("background-color", "#343a40");
}

function getAndShowSentMessages() {
    status = 2;
    callMessageApi('/~S4529439/api/message/get_sent_messages.php?page='+page);
    $('#dinamic-title').html("Messaggi Inviati");
    $('#openbtn').text("☰ Messaggi inviati");
    $("#get_unread").css("background-color", "#343a40");
    $("#get_received").css("background-color", "#343a40");
    $("#get_sent").css("background-color", "#676C72");
}

function showTable() {
    $('#page-selector').show();
    $('#message-table').show();
    $('#view-message').hide();
    $('#dinamic-title').show();
    closeNav();
}

function showMessage() {
    $('#page-selector').hide();
    $('#message-table').hide();
    $('#view-message').show();
    $('#dinamic-title').hide();
    closeNav();

}

function showError(text) {
    $('#errorMessage').text(text);
    $('#errorMessage').show();
}

function hideError() {
    $('#errorMessage').hide();
}

function callMessageApi(url) {
    let messages = new Object();
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'JSON',
        data: messages,
        success: function (data, textStatus, xhr) {
            let trHTML = "<tr> <th>ID</th> <th>Ricevuto il</th> <th>ID mittente</th> <th>Mittente</th> <th>Testo</th> <th>Priorità</th> </tr>";

            let itemNumber = 0;
            $.each(data, function (i, item) {
                itemNumber++;
                trHTML += '<tr class="clickable-row"><td>' + item.msg_id + '</td><td>' + item.msg_time + '</td><td>' + item.msg_sender.id + '</td><td class="link-to-profile">' + item.msg_sender.firstname + ' ' + item.msg_sender.lastname + '</td><td>' + item.msg_text.substr(0, 35) + '</td><td>' + item.msg_priority + '</td></tr>';
            });

            $('#message-table').html(trHTML);

            if(itemNumber<=0)
            {
                showError("Non ci sono messaggi");
            } else hideError();

            $(".clickable-row").click(function () {
                let value = $(this).find('td:eq(0)').text();
                getMessage(value);
            });
            showTable();

        },
        error: function (xhr, textStatus, errorThrown) {
            showError("Impossibile ottenere i messaggi");
        }
    });
}


function getMessage(id) {
    let message = new Object();
    //OTTENIMENTO MESSAGGIO
    $.ajax({
        url: '/~S4529439/api/message/get_message.php?id=' + id,
        type: 'GET',
        dataType: 'JSON',
        success: function (data, url, textStatus, xhr) {
            $('#sender').text("Messaggio da " + data.msg_sender.firstname + " " + data.msg_sender.lastname);
            $('#message-text').text(data.msg_text);
            $('#message-link').attr("href", './message/write.php?to=' + data.msg_sender.id);
            showMessage();
        },
        error: function (xhr, textStatus, errorThrown) {
            showError();
            console.log(xhr);
        }
    });
    //SET READ
    $.post("/~S4529439/api/message/set_message_read.php", { msg_id: id }, function(msg){
        //NESSUN FEEDBACK
    });


}

function openNav() {
    $('#sidebar').width("280px");
    $('#main-chat').css('margin-left',"280px");
}

function closeNav() {
    $('#sidebar').width(0);
    $('#main-chat').css('margin-left',0);
}

function hidePageSelector() {
        $('#page-selector').hide();
}

function showPageSelector() {
    $('#page-selector').show();
}