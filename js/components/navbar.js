let users = new Array();

$("#search").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: '/~S4529439/api/user/search_user.php?data=' + request.term,
                dataType: "json",
                data: users,
                success: function (data) {
                    let result = new Array();
                    $.each(data, function (i, item) {
                        result.push((new Object().suggest = (item.firstname + " " + item.lastname)));
                    });
                    response(result);
                },
                error: function (xhr) {

                }
            });
        },
        select: function (event, ui) {

        },
        minLength: 2
    }
);

$(window).resize(function () {
    searchShow();
});

function searchShow() {
    if (disabled) {
        $('#search-box').hide();
        $('#search-btn-alternative').hide();
    } else {
        if ($(window).width() > 1080 || $(window).width() < 768) {
            $('#search-box').show();
            $('#search-btn-alternative').hide();
        } else {
            $('#search-box').hide();
            $('#search-btn-alternative').show();
        }
    }

}