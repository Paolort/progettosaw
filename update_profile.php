<?php
//DEBUG
require_once 'utils/server/server_utils.php';
show_errors();
//CONTROLLA IL LOGIN
require_once 'service/user/user_service.php';
$auth_info = usr_srv_check_login(ADMIN_LVL);
//REDIRIGE SE NON LOGGATO
if (!$auth_info->authorized) {
    redirect(URL_UNAUTHORIZED);
}

global $message, $profile_data, $personal;
$personal = true; // SERVE PER IL FORM IMPORTATO
$message = update($auth_info->user_id);// MESSAGGIO PER IL FORM IMPORTATO
$profile_data = usr_srv_get_personal_data($auth_info->user_id); //dopo altrimenti visualizza i vecchi dati

?>

<!doctype html>
<html lang="it-it">
<head>
    <?php include_once 'components/head.php'; ?>
    <title>Profilo</title>
    <link href="./css/update_profile.css" rel="stylesheet" media="screen">
</head>
<body>
<?php include_once 'components/navbar.php';
include_once 'components/user_modify_form.php'
?>

<script src="./js/update_profile.js"></script>
<script type="text/javascript">
    document.getElementById("firstname").value = "<?php if (isset($profile_data->firstname)) echo $profile_data->firstname ?>";
    document.getElementById("lastname").value = "<?php if (isset($profile_data->lastname)) echo $profile_data->lastname ?>";
    document.getElementById("email").value = "<?php if (isset($profile_data->email)) echo $profile_data->email ?>";
    document.getElementById("bdate").value = "<?php if (isset($profile_data->b_date)) echo $profile_data->b_date?>";
    document.getElementById("description").value = "<?php if (isset($profile_data->description)) echo $profile_data->description?>";
</script>
</body>
</html>

<?php

/**
 * Permette di modificare il profilo utente
 * @param int $id l'id dell'utente alla quale cambiare i dati
 * @return string, query result message
 */
function update(int $id): string
{
    $to_return = "";
    if (isset($_POST["firstname"]) and isset($_POST["lastname"])) {
        if (isset($_POST["email"])) {
            $to_return = update_all($id);
        } else
            $to_return = update_names($id);
    }
    return $to_return;

}

/**
 * Permette di cambiare solamente nome e cognome
 * @param int $id l'id dell'utente alla quale cambiare i dati
 * @return string l'evte_my_profile_allentuale messaggio da stampare
 */
function update_names(int $id)
{
    $to_return = "";
    $firstname = trim($_POST["firstname"]);
    $lastname = trim($_POST["lastname"]);

    $err_msg = "";

    $err_msg = $err_msg.get_name_alert(check_name($firstname),"Nome");
    $err_msg = $err_msg.get_name_alert(check_name($lastname),"Cognome");



    if (empty($err_msg)) {
        $edit_result = usr_srv_update_name_surname($id, $firstname, $lastname);
        $to_return = print_message($edit_result);
    } else $to_return = $err_msg;
    return $to_return;
}

/**
 * Permette di cambiare i dati dell'utente
 * @param int $id l'id dell'utente alla quale cambiare i dati
 * @return string l'eventuale messaggio da stampare
 */
function update_all(int $id): string
{
    $firstname = trim($_POST["firstname"]);
    $lastname = trim($_POST["lastname"]);
    $email = strtolower(trim($_POST["email"]));
    $bdate = trim($_POST["bdate"]);
    $description = isset($_POST["description"]) ? $_POST["description"] : "";

    $err_msg = check_data_get_message_no_pwd($firstname,$lastname,$email,$bdate);


    if(empty($err_msg))
    {
        $edit_result = usr_srv_update_profile_all($id,$firstname, $lastname, $email, $bdate, $description);
        return print_message($edit_result);
    } else return $err_msg;
}

/**
 * Ritorna il messaggio di errore da stampare dato il codice di errore
 * @param $edit_result
 * @return string
 */
function print_message($edit_result): string
{
    switch ($edit_result) {
        case 0:
            $to_return = get_email_alert(3);
            break;
        case -1:
            $to_return = "<div class=\"alert alert-danger\" role=\"alert\">Non consentito</div>";
            break;
        case 1:
            $to_return = "<div class=\"alert alert-success\" role=\"alert\">Modifica effettuata con successo</div>";
            break;
        case -4:
            $to_return = "<div class=\"alert alert-danger\" role=\"alert\">I dati non sono variati</div>";
            break;
        case -5:
            $to_return = "<div class=\"alert alert-danger\" role=\"alert\">Errore exec</div>";
            break;
        default:
            $to_return = "<div class=\"alert alert-danger\" role=\"alert\">Errore sconosciuto: ".$edit_result."</div>";
    }
    return $to_return;

}

?>

