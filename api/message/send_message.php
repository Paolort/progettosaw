<?php
require_once '../../utils/server/server_utils.php';
show_errors();

require_once "../api_post_head.php";
require_once '../../classes/Message.php';
require_once '../../service/msg/msg_service.php';
require_once '../../service/user/user_service.php';

$auth_info;
if(isset($_GET["token"]))
{
    $auth_info = usr_srv_check_login_token($_GET["token"],USER_LVL);
}else {
    $auth_info = usr_srv_check_login(USER_LVL);
}

if($auth_info->authorized) {
    if(isset($_POST["text"]) and isset($_POST["receivers"])) {
        $text = $_POST["text"];
        $receivers = json_decode($_POST["receivers"]);

        $result = msg_srv_send_message($auth_info->user_id, $text, $receivers);
        $response = "";

        switch ($result) {
            case 1:
                http_response_code(200);
                $response = "Messaggio inviato con successo";
                break;
            case 0:
                http_response_code(400);
                $response = "400 Bad Request - Alcuni destinatari non esistono";
                break;
            case -1:
                http_response_code(500);
                $response = "400 Bad Request";
                break;
            case -2:
                http_response_code(400);
                $response = "400 Bad Request - Alcuni argomenti non sono corretti";
                break;
            default:
                http_response_code(400);
                $response = "500 - Internal server Error";
                break;
        }
    } else
    {
        http_response_code(400);
        $response = "400 Bad Request";
    }
    echo json_encode(
        array("message" => $response)
    );


}
else
{
    http_response_code(401);
    echo json_encode(
        array("message" => "Unauthorized")
    );
}