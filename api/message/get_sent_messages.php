<?php
require_once '../../utils/server/server_utils.php';
show_errors();

require_once "../api_get_head.php";
require_once '../../classes/Message.php';
require_once '../../service/msg/msg_service.php';
require_once '../../service/user/user_service.php';

$auth_info;
if(isset($_GET["token"]))
{
    $auth_info = usr_srv_check_login_token($_GET["token"],USER_LVL);
}else {
    $auth_info = usr_srv_check_login(USER_LVL);
}

if($auth_info->authorized) {
    $page = 0;
    if(isset($_GET["page"]) and is_numeric($_GET["page"]))
    {
        $page = intval($_GET["page"]);
    }

    // read the details of product to be edited

    $messages = msg_srv_get_sent_messages($auth_info->user_id,$page);

    // set response code - 200 OK
    http_response_code(200);

    // show products data
    echo json_encode($messages);
}
else
{
    http_response_code(401);
    echo json_encode(
        array("message" => "Unauthorized")
    );
}
