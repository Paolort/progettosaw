<?php
require_once '../../utils/server/server_utils.php';
show_errors();

require_once "../api_post_head.php";
require_once '../../classes/Message.php';
require_once '../../service/msg/msg_service.php';
require_once '../../service/user/user_service.php';

$auth_info;
if(isset($_GET["token"]))
{
    $auth_data = usr_srv_check_login_token($_GET["token"],USER_LVL);
}else {
    $auth_data = usr_srv_check_login(USER_LVL);
}

if($auth_data->authorized) {
    if(isset($_POST["msg_id"]) and !empty($_POST["msg_id"]) and is_numeric($_POST["msg_id"]))
    {
        try {
            $done = msg_rcv_set_read_message($auth_data->user_id, $_POST["msg_id"]);

            if ($done)
                http_response_code(200);
            else {
                http_response_code(304);
                echo json_encode(array("message" => "Non è stato possibile cancellare il messaggio"));
            }
        } catch (Error $e)
        {
            http_response_code(500);
        }
    }
    else
    {
        http_response_code(400);
        echo json_encode(
            array("message" => "Bad request, missing message id. It must be an int value")
        );
    }


}
else
{
    http_response_code(401);
    echo json_encode(
        array("message" => "Unauthorized")
    );
}
