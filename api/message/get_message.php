<?php
require_once '../../utils/server/server_utils.php';
show_errors();

require_once "../api_get_head.php";
require_once '../../classes/Message.php';
require_once '../../service/msg/msg_service.php';
require_once '../../service/user/user_service.php';

$auth_info;
if(isset($_GET["token"]))
{
    $auth_data = usr_srv_check_login_token($_GET["token"],USER_LVL);
}else {
    $auth_data = usr_srv_check_login(USER_LVL);
}

if($auth_data->authorized) {
    if(isset($_GET["id"]) and !empty($_GET["id"]) and is_numeric($_GET["id"]))
    {
        // read the details of product to be edited


        $message = msg_srv_get_message($_GET["id"],$auth_data->user_id);

        // set response code - 200 OK
        http_response_code(200);

        // show products data
        echo json_encode($message);
    }
    else
    {
        http_response_code(400);
        echo json_encode(
            array("message" => "Bad request, missing message id. It must be an int value")
        );
    }


}
else
{
    http_response_code(401);
    echo json_encode(
        array("message" => "Unauthorized")
    );
}
