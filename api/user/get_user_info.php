<?php
require_once '../../utils/server/server_utils.php';
show_errors();

require_once "../api_get_head.php";
require_once '../../classes/User.php';
require_once '../../service/user/user_service.php';

$auth_info;
if(isset($_GET["token"]))
{
    $auth_info = usr_srv_check_login_token($_GET["token"],USER_LVL);
}else {
    $auth_info = usr_srv_check_login(USER_LVL);
}

if($auth_info->authorized) {
    if(isset($_GET["id"]) and !empty($_GET["id"]) and is_numeric($_GET["id"]))
    {
        // read the details of product to be edited


        $user = usr_srv_get_profile_visit($_GET["id"]);

        // set response code - 200 OK
        http_response_code(200);

        // show products data
        echo json_encode($user);
    }
    else
    {
        http_response_code(400);
        echo json_encode(
            array("message" => "Bad request, missing message id. It must be an int value")
        );
    }
}
else
{
    http_response_code(401);
    echo json_encode(
        array("message" => "Unauthorized")
    );
}
