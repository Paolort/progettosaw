<?php
require_once '../../utils/server/server_utils.php';
show_errors();

require_once "../api_get_head.php";
require_once '../../classes/User.php';
require_once '../../service/user/user_service.php';

$auth_info;
if(isset($_GET["token"]))
{
    $auth_info = usr_srv_check_login_token($_GET["token"],USER_LVL);
}else {
    $auth_info = usr_srv_check_login(USER_LVL);
}

$response = "";
if($auth_info->authorized) {
    if(isset($_GET["data"]) and !empty($_GET["data"])) {
        $data = $_GET["data"];
        $result = usr_srv_search_user($data);

        if(count($result)>0)
        {
            echo json_encode($result);
        }
        else
        {
            http_response_code(404);
            $response = "404 l'utente cercato non è stato trovato.";
            echo json_encode(
                array("message" => $response)
            );
        }
    } else
    {
        http_response_code(400);
        $response = "400 Bad Request";
        echo json_encode(
            array("message" => $response)
        );
    }
}
else
{
    http_response_code(401);
    echo json_encode(
        array("message" => "Unauthorized")
    );
}