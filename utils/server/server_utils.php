<?php
const SHOW_ERRORS = false; //TODO

const URL_MAIN_PAGE = "/~S4529439";
const URL_LOGIN = "/~S4529439/login.php";
const URL_HOME = "/~S4529439/site";
const URL_UNAUTHORIZED = "/~S4529439/site/unauthorized.php";
const URL_USER_EDIT_PASSWORD = "/~S4529439/site/edit_password.php";
const URL_LOGOUT = "/~S4529439/site/logout.php";
const URL_PROFILE = "/~S4529439/show_profile.php";
const URL_REGISTER = "/~S4529439/registration.php";
const URL_ADMIN_EDIT_PASSWORD = "/~S4529439/site/administration/a_edit_password.php";
const URL_ADMIN_EDIT_USER = "/~S4529439/site/administration/a_edit_user.php";
const URL_ADMIN_USER_LIST = "/~S4529439/site/administration/user_list.php";
const URL_CHAT_HOME = "/~S4529439/site/chat";
const URL_SEARCH_USER = "/~S4529439/site/search_user.php";
const URL_ADMIN_HOME = "/~S4529439/site/administration";
const URL_UPDATE_PROFILE = "/~S4529439/update_profile.php";
const URL_MESSAGE_WRITE = "/~S4529439/site/chat/message/write.php";

/**
 * @param string $location, il percorso relativo alla root del server (Example: "site/administration.php")
 */
function redirect(string $location)
{
    header("Location: ".$location);
}

function show_errors()
{
    if(SHOW_ERRORS)
    {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
    }
}

function get_location() : string
{
    //return $_SERVER["DOCUMENT_ROOT"] . '/~S4529439'; //TODO
    return "/chroot/home/S4529439/public_html/";
}
