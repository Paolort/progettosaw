<?php
/**
 * Ritorna la password cifrata(hash). Non serve il SALT poichè ciò che questa funzione restituisce
 * lo incorpora. (USARE password_verify() per confrontare le password)
 * @inheritDoc https://www.php.net/manual/en/function.password-hash.php
 * @param $password, che deve essere cifrata. NB MAX LENGHT 72 CHAR
 * @return string HASH della password da salvare nel db
 */
function crypt_password($password) : string
{
    $to_return = null;
    if (strlen($password) <= 72)
        $to_return = password_hash($password, PASSWORD_BCRYPT);
    return $to_return;
}

/**
 * Controlla che la password sia nel formato corretto.
 * @param $password , stringa soddisfacente le condizioni
 * 1 - Almeno una minuscola
 * 2 - Almeno una maiuscola
 * 3 - Almeno un numero
 * 4 - Lunghezza compresa da 7 a 60 caratteri
 * @return int 0 se corretta, altrimenti PRIMA condizione violata. -1 if error.
 */
function check_password(string $password) : int
{
    $to_return = null;
    $lunghezza = strlen($password);
    if (!preg_match("#[a-z]+#", $password)) {
        $to_return = 1;
        //$passwordErr = "Your Password Must Contain At Least 8 Characters!";
    } elseif (!preg_match("#[A-Z]+#", $password)) {
        $to_return = 2;
        //$passwordErr = "Your Password Must Contain At Least 1 Number!";
    } elseif (!preg_match("#[0-9]+#", $password)) {
        $to_return = 3;
        //$passwordErr = "Your Password Must Contain At Least 1 Capital Letter!";
    } elseif ($lunghezza < 7 || $lunghezza > 60) {
        $to_return = 4;
        //$passwordErr = "Your Password Must Contain At Least 1 Lowercase Letter!";
    } else $to_return = 0;
    return $to_return;
}

/**
 * Stampa il messaggio di errore relativo al tipo di errore
 * @param int $code
 * 1 - Almeno una minuscola
 * 2 - Almeno una maiuscola
 * 3 - Almeno un numero
 * 4 - Lunghezza compresa da 7 a 60 caratteri
 * 5 - Le password non coincidono
 * @return string error message
 */
function get_password_alert(int $code) : string
{
    $to_return = "";
    switch ($code)
    {
        case 0:
            break;
        case 1:
            $to_return = "<div class=\"alert alert-danger\" role=\"alert\">La password deve contenere almeno una minuscola</div>";
            break;
        case 2:
            $to_return = "<div class=\"alert alert-danger\" role=\"alert\">La password deve contenere almeno una maiuscola</div>";
            break;
        case 3:
            $to_return = "<div class=\"alert alert-danger\" role=\"alert\">La password deve contenere almeno un numero</div>";
            break;
        case 4:
            $to_return = "<div class=\"alert alert-danger\" role=\"alert\">La password deve essere lunga dai 7 ai 60 caratteri</div>";
            break;
        case 5:
            $to_return = "<div class=\"alert alert-danger\" role=\"alert\">Le password non coincidono</div>";
            break;
        default:
            $to_return = "<div class=\"alert alert-danger\" role=\"alert\">Errore sconosciuto</div>";
    }
    return $to_return;
}

/**
 * Permette di sapere se il nome o cognome sono nel formato corretto.
 * @param $name
 * @return int code.
 * 0 - if correct
 * 1 - if empty
 * 2 - if incorrect char has been inserted
 * 3 - if too long
 */
function check_name(string $name) : int
{
    if(isset($name))
    {
        $lunghezza = strlen($name);
        if($lunghezza == 0)return 1;
        if($lunghezza > 60)return 3;
        if(!preg_match("/^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/", $name)) {
            return 2;
        }
        return 0;
    }
    return 1;
}

/**
 * Permette di ottenere uno stato sulla data che indica se  la data è corretta o meno.
 *
 * @param $bdate
 * @return int code.
 * 0 - if correct
 * 1 - if empty
 * 2 - if incorrect format
 * 3 - data non valida
 * 5 - if you come from future
 * 6 - Soglia minima di età non rispettata
 *
 */
function check_bdate(string $bdate) : int
{

    if(isset($bdate))
    {
        if(preg_match("([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))",$bdate)) {
            $date = strtotime($bdate); //in SECONDI
            if (isset($date)) {
                $diff = time() - $date;
                if ($diff < 0) return 5;
                else {
                    if ($diff > 441504000) return 0;
                    else return 6;
                }
            } else return 3; //PARSE FALLITO
        }
        else return 2;
    }
    else return 1;
}

/**
 * @param $email, la mail che deve essere controllata
 * @return int
 * 0 - corretta
 * 1 - vuota
 * 2 - formato non valido
 */
function check_email(string $email) : int
{
    //return filter_var($email,FILTER_VALIDATE_EMAIL);
    if(isset($email))
    {
        if(preg_match("/[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+.[a-zA-Z]{2,4}/",$email)) {
            return 0;
        }
        else return 2;
    }
    else return 1;
}


/**
 * @param int $code
 * 0 - if correct
 * 1 - if empty
 * 2 - if incorrect char has been inserted
 * 3 - if too long
 * @param string $nomeCampo
 * @return string error message
 */
function get_name_alert(int $code, string $nomeCampo) : string
{
    $to_return = "";
    switch ($code)
    {
        case 0:
            break;
        case 1:
            $to_return = "<div class=\"alert alert-danger\" role=\"alert\">Il campo ". $nomeCampo . " non può essere vuoto</div>";
            break;
        case 2:
            $to_return = "<div class=\"alert alert-danger\" role=\"alert\">Nel campo ". $nomeCampo . " è presente un carattere non consentito</div>";
            break;
        case 3:
            $to_return = "<div class=\"alert alert-danger\" role=\"alert\">Il campo ". $nomeCampo . " non può contenere più di 60 caratteri</div>";
            break;
        default:
            $to_return = "<div class='errorMessage'>Errore sconosciuto</div>";
    }
    return $to_return;
}


/**
 * @param int $code
 * 0 - if correct and is 18 yo
 * 1 - if empty
 * 2 - if incorrect format
 * 3 - data non valida
 * 4 - if correct but NOT 18 yo
 * 5 - if you come from the fucking future
 * @return string error message
 */
function get_data_alert(int $code) : string
{
    $to_return = "";
    switch ($code)
    {
        case 0:
            break;
        case 1:
            $to_return = "<div class=\"alert alert-danger\" role=\"alert\">La data di nascita non può essere vuota</div>";
            break;
        case 2:
            $to_return = "<div class=\"alert alert-danger\" role=\"alert\">La data inserita non è un formato valido</div>";
            break;
        case 3:
            $to_return = "<div class=\"alert alert-danger\" role=\"alert\">La data inserita non è valida</div>";
            break;
        case 5:
            $to_return = "<div class=\"alert alert-danger\" role=\"alert\">Vieni dal futuro? :O</div>";
            break;
        case 6:
            $to_return = "<div class=\"alert alert-danger\" role=\"alert\">Non superi l'età minima!</div>";
            break;
        default:
            $to_return = "<div class='errorMessage'>Errore sconosciuto</div>";
    }
    return $to_return;
}

/**
 * @param $code
 * 0 - corretta
 * 1 - vuota
 * 2 - formato non valido
 * 3 - Mail già presente nel sistema
 * @return string messaggio di errore
 */
function get_email_alert(int $code) : string
{
    $to_return = "";
    switch ($code)
    {
        case 0:
            break;
        case 1:
            $to_return = "<div class=\"alert alert-danger\" role=\"alert\">L'email non può essere vuota'</div>";
            break;
        case 2:
            $to_return = "<div class=\"alert alert-danger\" role=\"alert\">Il formato dell'email non è valido</div>";
            break;
        case 3:
            $to_return = "<div class=\"alert alert-danger\" role=\"alert\">La mail è già presente nel sistema</div>";
            break;
        default:
            $to_return ="<div class='errorMessage'>Errore sconosciuto</div>";
    }
    return $to_return;
}

/**
 * Controlla i dati e restituisce un messaggio con i vari errori. Non controlla le password
 *
 * @param string $firstname
 * @param string $lastname
 * @param string $email
 * @param string $bdate
 * @return string il messaggio di errore dei dati già formattato in html.
 */
function check_data_get_message_no_pwd(string $firstname, string $lastname, string $email, string $bdate) :string
{
    $err_msg = "";

    $err_msg = $err_msg.get_name_alert(check_name($firstname),"Nome");
    $err_msg = $err_msg.get_name_alert(check_name($lastname),"Cognome");
    $err_msg = $err_msg.get_email_alert(check_email($email));
    $err_msg = $err_msg.get_data_alert(check_bdate($bdate));

    return $err_msg;
}

