<?php

/**
 * Invia un messaggio a uno o più utenti.
 * @param int $sender_id l'id del mittente del messaggio
 * @param $text ,Il testo del messaggio
 * @param $receivers l'id dei destinatari del messaggio
 * @return int code
 * 1 - Messaggio inviato con successo
 * 0 - Uno o più destinatari non esistono
 * -1 - Errore durante l'invio
 * -2 - Dati richiesta non impostati
 * -3 - Binding dati fallito
 * -4 - Connessione al bd fallita
 */
function msg_srv_send_message(int $sender_id, string $text, array $receivers): int
{
    if (isset($text) and isset($receivers)) {
        $con = (new Database())->getConnection();
        if ($con != null) {
            $num_dest = 0;
            $stmt = $con->prepare("SELECT usr_id FROM user WHERE usr_id = :id");
            foreach ($receivers as $id) {
                if (!$stmt->bindParam(":id", $id, PDO::PARAM_INT)) return -3;
                else {
                    $stmt->execute();
                    $num_row = $stmt->rowCount();
                    if ($num_row > 0) $num_dest++;
                }
            }

            //Se tutti i destinatari esistono, altrimenti non esegue
            if ($num_dest == count($receivers)) {

                $con->beginTransaction();
                $stmt2 = $con->prepare("INSERT INTO message (msg_text,msg_sender) VALUES (:text, :sender)");
                $stmt2->bindParam(":text", $text, PDO::PARAM_STR);
                $stmt2->bindParam(":sender", $sender_id, PDO::PARAM_STR);

                $stmt2->execute();

                if ($stmt->rowCount() > 0) //SE VIENE ESEGUITO CON SUCCESSO
                {
                    $num_msg_succes_sent = 0;
                    $msg_id = $con->lastInsertId();
                    $stmt = $con->prepare("INSERT INTO `receive` (`rcv_msg_id`,`rcv_user_id`) VALUES (:nmsg_id, :nuser_id)");
                    foreach ($receivers as $id) {
                        $bind_err = false;
                        $bind_err = $bind_err or !$stmt->bindParam(":nmsg_id", $msg_id, PDO::PARAM_INT);
                        $bind_err = $bind_err or !$stmt->bindParam(":nuser_id", $id, PDO::PARAM_INT);
                        if ($bind_err) {
                            $con->rollBack();
                            return -3;
                        } else {
                            $stmt->execute();
                            $num_row = $stmt->rowCount();
                            if ($num_row > 0) $num_msg_succes_sent++;
                        }
                    }

                    if ($num_msg_succes_sent == count($receivers)) {
                        $con->commit();
                        return 1;
                    } else {
                        //Il messaggio non è stato inviato a tutti i destinatari
                        $con->rollBack();
                        return -1;
                    }
                } else {
                    //Il messaggio non è stato inserito
                    $con->rollBack();
                    return -1;
                }
            } else return 0; //NON ESISTONO TUTTI I DESTINATARI
        } else return -4; //No connessione al database
    } else return -2; //Dati in input non impostati
}

/**
 * Permette di ottenere un messaggio se l'utente ne ha diritto.
 *
 * @param int $message_id l'id del messaggio da ottenere
 * @param int $user_id l'utente che richiede il messaggio.
 * @return Message msg, se è stato possibile ottenerlo, null se non si ha i privilegi di ottenerlo,
 * i parametri non sono settati o c'è stato un errore
 */
function msg_srv_get_message(int $message_id, int $user_id): Message
{
    if (!isset($message_id) or !isset($user_id)) return null;

    $con = (new Database())->getConnection();
    if ($con != null) {
        //Ottiene il messaggio solo se è stato inviato o ricevuto dall'utente che lo richiede
        $stmt = $con->prepare("SELECT msg_id,msg_text,msg_priority,msg_time,msg_deleted_by_sender,user.usr_id,user.usr_firstname,user.usr_lastname FROM message INNER JOIN user ON message.msg_sender = user.usr_id WHERE msg_id=:msgid AND (:usrid IN (SELECT rcv_user_id FROM receive WHERE rcv_msg_id = :msgid) OR message.msg_sender = :usrid)");
        $bind_err = false;
        $bind_err = $bind_err or $stmt->bindParam(":msgid", $message_id, PDO::PARAM_INT);
        $bind_err = $bind_err or $stmt->bindParam(":usrid", $user_id, PDO::PARAM_INT);
        if ($bind_err) return null;

        if ($stmt->execute()) {
            if($stmt->rowCount()>0) { //SE C'È il messaggio
                //Il risultato ha una sola linea
                $row = $stmt->fetch(PDO::FETCH_ASSOC);

                $message = new Message($row);

                $user = new User($row);

                $message->msg_sender = $user;

                return $message;
            } else
                return new Message();
        }
    }
    return new Message(); //CONNESSIONE NULLA O NON ESEGUITO
}

/**
 * Ritorna i propri messaggi, 20 per pagina
 * @param int $page_number , il numero della pagina da prenderere
 * @param int $id l'id dell'utente che desidera vedere i propri messaggi
 * @param bool $only_unread true se si desiderano solo i messaggi non letti
 * @return array contenente i messaggi da leggere. Se non ci sono messaggi, le pagine sono esaurite
 * o c'è stato un errore restituisce null.
 */
function msg_srv_get_messages(int $id, int $page_number, bool $only_unread): array
{
    if (isset($id)) {
        $con = (new Database())->getConnection();
        if ($con != null) {

            $start = $page_number * 20;
            if ($only_unread)
                $stmt = $con->prepare("SELECT msg_id,msg_text,msg_sender,msg_time,msg_priority,user.usr_firstname as sender_firstname,user.usr_lastname as sender_lastname FROM message INNER JOIN user ON message.msg_sender=user.usr_id WHERE msg_id IN (SELECT rcv_msg_id FROM receive WHERE rcv_user_id = :id AND rcv_read = 0) ORDER BY message.msg_time DESC LIMIT 20 OFFSET :start");
            else
                $stmt = $con->prepare("SELECT msg_id,msg_text,msg_sender,msg_time,msg_priority,user.usr_firstname as sender_firstname,user.usr_lastname as sender_lastname FROM message INNER JOIN user ON message.msg_sender=user.usr_id WHERE msg_id IN (SELECT rcv_msg_id FROM receive WHERE rcv_user_id = :id) ORDER BY message.msg_time DESC LIMIT 20 OFFSET :start");
            $bind_err = false;
            $bind_err = $bind_err or $stmt->bindParam(":id", $id, PDO::PARAM_INT);
            $bind_err = $bind_err or $stmt->bindParam(":start", $start, PDO::PARAM_INT);

            $executed = $stmt->execute();

            if ($executed and $stmt->rowCount() > 0) {
                $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $to_return = array();
                foreach ($result as $row) {
                    $tmp_msg = new Message($row);
                    $sender = new User();
                    $sender->id = $row[DB_MSG_SENDER];
                    $sender->firstname = $row["sender_firstname"];
                    $sender->lastname = $row["sender_lastname"];

                    $tmp_msg->msg_sender = $sender;
                    array_push($to_return, $tmp_msg);
                }
                return $to_return;
            }
            //echo "RIGHE:".$stmt->rowCount()." ESEGUITO:".$executed." BINDERR".$bind_err." ERRORMSG:".json_encode($stmt->errorInfo())."\n\n";
        }
    }
    return array();
}

/**
 * Imposta un messaggio come letto.
 * @param int $id l'id dell'utente che effettua la richiesta
 * @param int $message_id l'id del messaggio da impostare come letto
 * @return true se è stato impostato come letto.
 */
function msg_rcv_set_read_message(int $id, int $message_id): bool
{
    $to_return = false;
    $con = (new Database())->getConnection();
    if ($con != null) {
        try {
            $stmt = $con->prepare("UPDATE receive SET rcv_read = 1 WHERE rcv_msg_id = :msgid AND rcv_user_id = :usrid");
            $stmt->bindParam(":msgid", $message_id);
            $stmt->bindParam(":usrid", $id);
            $stmt->execute();

            if ($stmt->rowCount() > 0) $to_return = true;
        } catch (PDOException $e) {

        }
    }
    return $to_return;
}

/**
 * Restituisce i messaggi inviati
 * @param int $id l'id dell'utente quale recuperare i messaggi inviati.
 * @param $page_number , il numero di pagina
 * @return array, i messaggi inviati
 */
function msg_srv_get_sent_messages(int $id, int $page_number): array
{
    $to_return = array();
    if (isset($id)) {
        $con = (new Database())->getConnection();
        if ($con != null) {
            try {
                $start = $page_number * 20;
                $stmt = $con->prepare("SELECT msg_id,msg_text,msg_sender,msg_time,msg_priority,user.usr_id,user.usr_firstname,user.usr_lastname FROM message INNER JOIN user ON message.msg_sender = user.usr_id WHERE msg_sender = :id ORDER BY message.msg_time DESC LIMIT 20 OFFSET :start");
                $stmt->bindParam(":id", $id, PDO::PARAM_INT);
                $stmt->bindParam(":start", $start, PDO::PARAM_INT);

                $stmt->execute();

                if ($stmt->rowCount() <= 0) return $to_return;

                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $message = new Message($row);
                    $user = new User($row);
                    $message->msg_sender = $user;
                    array_push($to_return, $message);
                }

                $stmt = null; //CLOSE CONNECTION

            } //ERRORE DI ESEGUZIONE
            catch (PDOException $e) {
                $to_return = null;
            }
        } else $to_return = null;
        $con = null;
    }
    return $to_return;
}


