<?php

//require_once $_SERVER["DOCUMENT_ROOT"] . '/~S4529439/utils/server/server_utils.php';
require_once '/chroot/home/S4529439/public_html/utils/server/server_utils.php';

require_once get_location() . '/utils/user/user_utils.php';
require_once get_location() . '/classes/User.php';
require_once get_location() . '/classes/AuthInfo.php';
require_once get_location() . '/db/Database.php';
require_once get_location() . '/db/mysql_credentials.php';
require_once get_location() . '/service/user/user_service.php';
const MAX_USER_PER_LIST = 30;

/**
 * Restituisce le credenziali salvate nei cookie
 * @return array con [0]=id & [1]=token
 * Se il cookie "auth" non è settato o uno dei due dati risulta vuoto ritorna null.
 */
function usr_srv_get_credentials(): array
{
    $to_return = array();
    if (isset($_SESSION["token"]) and isset($_SESSION["id"])) {
        $token = $_SESSION["token"];
        $id = $_SESSION["id"];
        $to_return = array($id, $token);
    }
    return $to_return;
}

/**
 * Controlla se il token client side corrisponde a quello lato server e che non sia scaduto.
 * NB Prende id e token dalla variabile di SESSIONE.
 * @param $required_auth_lvl , Il livello di autorizzazione richiesto
 * @return AuthInfo l'informazione sull'avvenuto login.
 */
function usr_srv_check_login(int $required_auth_lvl): AuthInfo
{
    $to_return = new AuthInfo();
    session_start();
    $credentials = usr_srv_get_credentials();

    if (isset($credentials) and !empty($credentials)) {
        $id = $credentials[0];
        $to_return->user_id = $id;
        $token = $credentials[1];

        $con = (new Database())->getConnection();
        if ($con != null) {
            $stmt = $con->prepare("SELECT usr_auth_lvl, usr_token, usr_expires FROM user WHERE usr_id = :id");
            $stmt->bindParam(":id", $id, PDO::PARAM_INT);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);  //IL RISULTATO DEVE AVERE UNA SOLA LINEA, WHERE SU PRIMARY KEY

            $to_return->user_auth_lvl = $db_authlvl = $row[DB_USR_AUTH_LVL];
            $db_token = $row[DB_USR_TOKEN];
            $db_expires = $row[DB_USR_EXPIRES];
            $diff = strtotime($db_expires) - time();


            if ($db_token != null and $db_token == $token and $diff > 0) {
                //CONTROLLO LIVELLO AUTH
                if (isset($required_auth_lvl)) {
                    if ($db_authlvl >= $required_auth_lvl) $to_return->authorized = true;
                    else $to_return->authorized = false; //INSUFFICIENT PERMISSION
                    //IN OGNI CASO COMUNQUE È LOGGATO
                    $to_return->logged = true;
                }
            } else {
                //IL TOKEN NON CORRISPONDE O È SCADUTO
                if ($diff <= 0) usr_srv_local_logout();
            }
        } else {
            $to_return->authorized = false; //CONNESSIONE NON RIUSCITA AL DB
        }
    }
    return $to_return;
}

/**
 * Controlla se il token client side corrisponde a quello lato server e che non sia scaduto.
 * NB Il token viene fornito ESTERNAMENTE
 *
 * @param string $token il token da controllare
 *
 * @param int $required_auth_lvl , Il livello di autorizzazione richiesto
 *
 * @return AuthInfo l'informazione sull'avvenuto login
 */
function usr_srv_check_login_token(string $token, int $required_auth_lvl): AuthInfo
{
    $con = (new Database())->getConnection();
    if ($con != null) {
        $stmt = $con->prepare("SELECT usr_id, usr_auth_lvl, usr_expires FROM user WHERE usr_token = :token");
        if (!$stmt->bindParam(":token", $token, PDO::PARAM_STR)) return new AuthInfo(); //BIND FALLITO

        if ($stmt->execute()) {

            if ($stmt->rowCount() <= 0) return new AuthInfo(); //NON CI SONO RISCONTRI

            $row = $stmt->fetch(PDO::FETCH_ASSOC);  //IL RISULTATO DEVE AVERE UNA SOLA LINEA, WHERE SU PRIMARY KEY

            $stmt = null; //CLOSE CONNECTION

            $db_id = $row[DB_USR_ID];
            $db_authlvl = $row[DB_USR_AUTH_LVL];
            $db_expires = $row[DB_USR_EXPIRES];
            $diff = strtotime($db_expires) - time();

            if (isset($diff) && isset($required_auth_lvl)) {
                $diff = strtotime($db_expires) - time();
                //CONTROLLO LIVELLO AUTH
                if ($diff > 0) {
                    $to_return = new AuthInfo();
                    if ($db_authlvl >= $required_auth_lvl) { //ACCESSO VERIFICATO
                        $to_return->authorized = true; //OK
                    }
                    $to_return->user_auth_lvl = $db_authlvl;
                    $to_return->user_id = $db_id;
                    $to_return->logged = true; //IN OGNI CASO È LOGGATO MA NON PER FORZA HA I PERMESSI SUFFICIENTI
                    return $to_return;
                }
            }
        }
    }
    return new AuthInfo();
}

/**
 * Funzione che presi pass e email in ingresso controlla se nel db è presente una tupla corrispondente.
 * Se è presente imposta il cookie "auth" client side contenente id::token e salva sul db il token.
 * @param string $email l'email con la quale effettuare il login
 * @param string $password la password con la quale effettuare il login.
 * @return bool true se l'autenticazione ha avuto successo, false altrimenti
 * @throws Exception se non in grado di collezzionare abbastanza entropia
 */
function usr_srv_login(string $email, string $password): bool
{
    $to_return = false;
    $stmt = null;
    $con = (new Database())->getConnection();
    if ($con != null) {
        $stmt = $con->prepare("SELECT usr_id,usr_pwd FROM user WHERE usr_email = :email");
        if (!$stmt->bindParam(":email", $email, PDO::PARAM_STR)) return false; //BINDING NON RIUSCITO

        if ($stmt->execute()) {
            $row = $stmt->fetch(); // il risultato ha una sola linea
            $id = (int)$row[DB_USR_ID];
            $db_hash = $row[DB_USR_PWD];
            $pass_equals = password_verify($password, $db_hash);
            if (isset($id) && $pass_equals) {
                $tmptoken = bin2hex(random_bytes(32));
                $stmt = $con->prepare('UPDATE user SET usr_token = :token, usr_expires = (CURDATE() + INTERVAL 1 month) WHERE usr_id = :id');

                $err = false;
                $err = $err or !$stmt->bindParam(":token", $tmptoken, PDO::PARAM_STR);
                $err = $err or !$stmt->bindParam(":id", $id, PDO::PARAM_INT);
                if (!$err and $stmt->execute()) {
                    $numrow = $stmt->rowCount();
                    if ($numrow > 0) {
                        $_SESSION["token"] = $tmptoken;
                        $_SESSION["id"] = $id;
                        $to_return = true;
                    }
                }
            }
        }
    }
    return $to_return;
}

/**
 * Funzione di registrazione. Se l'email non è già presente procede nell'aggiungere l'utente al database.
 * Non serve transazione poichè email è UNIQUE e il db non permette inserimento email già presente.
 *
 * @param string $firstname
 * @param string $lastname
 * @param string $email
 * @param string $password
 * @return int codice di ritorno
 * -3 == Errore di binding dati.
 * -2 == Connessione al database non riuscita
 * -1 == Errore di registrazione
 * 0 == Mail già presente
 * 1 == successo
 */ //TEST SE OR FUNZIONA CON BIND.
function usr_srv_register($firstname, $lastname, $email, $password)
{
    $password = crypt_password($password);
    $con = (new Database())->getConnection();
    if ($con != null) {
        $stmt = $con->prepare("SELECT COUNT(usr_id) AS conta FROM user WHERE usr_email = :email");
        if (!$stmt->bindParam(":email", $email, PDO::PARAM_STR)) return -3;

        if ($stmt->execute()) {
            $row = $stmt->fetch();
            $conta = $row['conta'];

            //SE NON CI SONO ALTRI UTENTI CON LA STESSA EMAIL
            if ($conta == 0) {
                $stmt = $con->prepare('INSERT INTO user (usr_firstname, usr_lastname, usr_email, usr_pwd) VALUES (:first_name, :last_name, :email, :password)');

                $err = false;
                $err = $err or !$stmt->bindParam(":first_name", $firstname, PDO::PARAM_STR);
                $err = $err or !$stmt->bindParam(":last_name", $lastname, PDO::PARAM_STR);
                $err = $err or !$stmt->bindParam(":email", $email, PDO::PARAM_STR);
                $err = $err or !$stmt->bindParam(":password", $password, PDO::PARAM_STR);
                if ($err) return -1;

                if (!$stmt->execute()) return -1;
                $numrow = $stmt->rowCount();
                if ($numrow > 0) {
                    return 1; //AGGIUNTA EFFETTUATA
                } else return -1;
            } else
                return 0; //MAIL GIÀ PRESENTE
        } else return -1; // NO CONNESSIONE
    } else return -2;
}


/**
 * Funzione di logout del sistema. Invia la richiesta di cancellazione del token lato server, successivamente rimuove il token lato client.
 * Se la richiesta non va a buon fine il token non viene cancellato lato client.
 * @param int $id , l'id dell'utente alla quale effettuare il logout
 * @return int code,
 * 1 - Logout effettuato con successo
 * -1 - Non riuscito
 */
function usr_srv_logout(int $id): int
{
    $to_return = 0;
    $stmt = null;
    $con = (new Database())->getConnection();
    if ($con != null) {
        try {
            $stmt = $con->prepare('UPDATE user SET usr_token = NULL , usr_expires = (CURDATE() - INTERVAL 1 month) WHERE usr_id = :id');
            $stmt->bindParam(":id", $id, PDO::PARAM_INT);
            $stmt->execute();
            usr_srv_local_logout();
            $to_return = 1;
            $stmt = null;
        } catch (PDOException $e) {
            $stmt = null;
            $to_return = -1;
        }
    }
    $con = null;
    return $to_return;
}

/**
 * Funzione di cancellazione lato client del token.
 * Viene invocato quando viene effettuato l'accesso da un dispositivo con token scaduto,un tentativo di irruzzione, cancella solo i dati locali altrimenti cancellerebbe
 * le sessioni degli utenti senza verificare che effettivamente siano essi.
 */
function usr_srv_local_logout()
{
    session_destroy();
}

/**
 * Ritorna la lista degli utenti
 * @param int $page
 * @return array, an array containing a lst of user, otherwise il contains an int with error status value
 * -1 - Connection error
 * -2 - Errore durante la richiesta
 */
function usr_srv_a_user_list(int $page): array
{
    $to_return = array();
    $con = (new Database())->getConnection();
    if ($con != null) {
        try {
            $stmt = $con->prepare("SELECT usr_id,usr_firstname,usr_lastname,usr_email,usr_mail_verified,usr_auth_lvl,usr_description,usr_b_date FROM user LIMIT " . MAX_USER_PER_LIST . " OFFSET " . $page * MAX_USER_PER_LIST);
            $stmt->execute();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $user = new User($row);
                array_push($to_return, $user);
            }
            $stmt = null;
        } catch (PDOException $e) {
            $stmt = null;
            $to_return = array(-2);
        }
    } else $to_return = array(-2);
    $con = null;
    return $to_return;
}

/**
 * Permette di ottenere i prorpi dati personali
 * @param int $id dell'utente da reperirne i dati
 * @return User l'utente desiderato altrimenti null se la connessione non è riuscita oppure
 * se i dati dell'utente non sono stati trovati i suoi dati (no match id)
 */
function usr_srv_get_personal_data(int $id): User
{
    return usr_srv_a_get_user_data($id);
}

/**
 * Ritorna i dati di un utente. Metodo utile per amministratori.
 * @param int $id dell'utente da reperirne i dati
 * @return User l'utente desiderato altrimenti null se la connessione non è riuscita oppure
 * se i dati dell'utente non sono stati trovati i suoi dati (no match id)
 */
function usr_srv_a_get_user_data(int $id): User
{
    $stmt = null;
    $con = (new Database())->getConnection();
    if ($con) {
        $stmt = $con->prepare("SELECT usr_id,usr_firstname,usr_lastname,usr_email,usr_mail_verified,usr_auth_lvl,usr_description,usr_b_date FROM user WHERE usr_id=:id");
        if (!$stmt->bindParam(":id", $id, PDO::PARAM_INT)) return null;
        if ($stmt->execute()) {
            $row = $stmt->fetch();
            return new User($row);
        } else return null;
    }
    return null;
}

/**
 * Funzione di modifica dati di un utente. Funzione riservata ad uso amministratore.
 *
 * @param int $id l'id dell'utente sulla quale effettuare le modifiche
 * @param string $firstname il nome nuovo
 * @param string $lastname il cognome nuovo
 * @param string $email la nuova mail
 * @param string $b_date nuova data di compleanno
 * @param string $description nuova descrizione
 * @return int code
 * -4 = Connessione non riuscita
 * -3 = Bind dati fallito
 * -2 = I dati sono rimasti gli stesso
 * -1 = se c'è stato un errore
 * 0 = la mail è già presente
 * 1 = operazione effettuata con successo
 */
function usr_srv_a_modifyUser($id, $firstname, $lastname, $email, $b_date, $description)
{
    $con = (new Database())->getConnection();
    if ($con != null) {
        //PARTE DI CONTROLLO PRESENZA UTENTE
        $stmt = $con->prepare("SELECT usr_email FROM user WHERE usr_email = :email");
        if (!$stmt->bindParam(":email", $email, PDO::PARAM_STR)) return -3;

        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC); // il risultato ha una sola linea
        $conta = $stmt->rowCount();

        $db_mail = $row[DB_USR_EMAIL];

        //SE NON SONO PRESENTI ALTRE MAIL O LA MAIL E' LA STESSA DI PRIMA
        if ($conta == 0 or strcmp($db_mail, $email) == 0) {
            $stmt = $con->prepare("UPDATE user SET usr_firstname=:firstname,usr_lastname=:lastname,usr_email=:email,usr_description=:description,usr_b_date=:date  WHERE usr_id=:id");
            $bind_err = false;
            $bind_err = $bind_err or !$stmt->bindParam(":firstname", $firstname, PDO::PARAM_STR);
            $bind_err = $bind_err or !$stmt->bindParam(":lastname", $lastname, PDO::PARAM_STR);
            $bind_err = $bind_err or !$stmt->bindParam(":email", $email, PDO::PARAM_STR);
            $bind_err = $bind_err or !$stmt->bindParam(":description", $description, PDO::PARAM_STR);
            $bind_err = $bind_err or !$stmt->bindParam(":date", $b_date, PDO::PARAM_STR);
            $bind_err = $bind_err or !$stmt->bindParam(":id", $id, PDO::PARAM_INT);
            if ($bind_err) return -3; //BIND ERROR
            else {
                $executed = $stmt->execute();
                if (!$executed) {
                    return -1;
                } else {
                    $affected = $stmt->rowCount();
                    if ($affected >= 1) {
                        return 1; //OK
                    } else {
                        return -2; //SE NON SONO STATE MODIFICATE RIGHE
                    }
                }
            }
        } else {
            return 0; //MAIL GIA PRESENTE
        }
    } else return -4; //NO CONNESSIONE
}

/**
 * Modifica del PROPRIO profilo da parte di un utente.
 * Modifica i dati di un utente, la mail non deve essere assegnata a qualche altro utente.
 *
 * @param int $id l'id dell'utente sulla quale effettuare la modifica
 * @param string $firstname il vecchio o nuovo nome
 * @param string $lastname il vecchio o nuovo cognome
 * @param string $email la vecchia o nuova mail con le condizione sopra
 * @param string $b_date la data di compleanno
 * @param string $description la descrizione
 * @return int code codice di ritorno contenente informazioni su errori eventuali.
 * -5 = Errore eseguzione
 * -4 = I dati non sono cambiati
 * -3 = Errore bind dati
 * -2 = connessione al db non riuscita
 * -1 = non consentito
 * 0 = mail già presente
 * 1 = modifica effettuata
 */
function usr_srv_update_profile_all(int $id, string $firstname, string $lastname, string $email, string $b_date, string $description): int
{
    $con = (new Database())->getConnection();
    if ($con != null) {
        //PARTE DI CONTROLLO PRESENZA UTENTE
        $stmt = $con->prepare("SELECT usr_email FROM user WHERE usr_email = :email");
        if (!$stmt->bindParam(":email", $email, PDO::PARAM_STR)) return -3; //BIND FALLITO

        $stmt->execute();

        $row = $stmt->fetch(); // il risultato ha una sola linea
        $conta = $stmt->rowCount();
        if($conta>0)
        {
            $db_mail = $row["usr_email"];
            if(strcmp($db_mail, $email) == 0)$conta = 0; //SE LA MAIL E' LA STESSA
        }

        //SE NON SONO PRESENTI ALTRE MAIL OPPURE NON è cambiata
        if ($conta == 0) {
            $stmt = $con->prepare("UPDATE user SET usr_firstname=:firstname ,usr_lastname=:lastname, usr_email=:email, usr_description=:description, usr_b_date=:b_date WHERE usr_id=:id");
            $bind_err = false;
            $bind_err = $bind_err or $stmt->bindParam(":firstname", $firstname, PDO::PARAM_STR);
            $bind_err = $bind_err or $stmt->bindParam(":lastname", $lastname, PDO::PARAM_STR);
            $bind_err = $bind_err or $stmt->bindParam(":email", $email, PDO::PARAM_STR);
            $bind_err = $bind_err or $stmt->bindParam(":description", $description, PDO::PARAM_STR);
            $bind_err = $bind_err or $stmt->bindParam(":b_date", $b_date, PDO::PARAM_STR);
            $bind_err = $bind_err or $stmt->bindParam(":id", $id, PDO::PARAM_INT);
            if ($bind_err) return -3;

            if ($stmt->execute()) {
                $affected = $stmt->rowCount();
                if ($affected == 1) {
                    return 1; //OK
                } else {
                    return -4; //DATI NON SONO VARIATI
                }
            } else
            {
                //echo json_encode($stmt->errorInfo());
                return -5;
            } //ERRORE ESEGUZIONE
        } else {
            return 0; //MAIL GIÀ PRESENTE
        }
    }
    return -2; //CONNESSIONE NON RIUSCITA
}


/**
 * Modifica del PROPRIO profilo da parte di un utente.
 * Modifica i dati di un utente, la mail non deve essere assegnata a qualche altro utente.
 *
 * @param int $id l'id dell'utente sulla quale effettuare la modifica
 * @param string $firstname il vecchio o nuovo nome
 * @param string $lastname il vecchio o nuovo cognome
 * @return int code codice di ritorno contenente informazioni su errori eventuali.
 * -5 = Errore sconosciuto
 * -4 = I dati non sono cambiati
 * -3 = Bind dati fallito.
 * -2 = connessione al db non riuscita
 * -1 = non consentito
 * 1 = modifica effettuata
 */
function usr_srv_update_name_surname(int $id, string $firstname, string $lastname): int
{
    $con = (new Database())->getConnection();
    if ($con != null) {
        $stmt = $con->prepare("UPDATE user SET usr_firstname=:firstname ,usr_lastname=:lastname WHERE usr_id=:id");

        $bind_err = false;
        $bind_err = $bind_err or !$stmt->bindParam(":firstname", $firstname, PDO::PARAM_STR);
        $bind_err = $bind_err or !$stmt->bindParam(":lastname", $lastname, PDO::PARAM_STR);
        $bind_err = $bind_err or !$stmt->bindParam(":id", $id, PDO::PARAM_INT);
        if ($bind_err) return -3; //BIND FALLITO

        if ($stmt->execute()) {
            $affected = $stmt->rowCount();
            if ($affected == 1) {
                return 1; //OK
            } else {
                return -4; //RIGHE NON MODIFICATE MA NO ERRORI => I DATI ERANO IDENTICI1
            }
        } else {
            return -5; //QUERY NON ESEGUITA
        }
    }
    return -2; //CONN NON RIUSCITA
}

/**
 * Funzione di modifica della propria password.
 * @param int $id L'id dell'utente alla quale modificare la password
 * @param string $new_password la nuova password desiderata
 * @param string $old_password la vecchia password, deve combaciare con quella presente sul database
 * @return int code
 * 1 - password cambiata
 * 0 - password nulla
 * -1 - errore durante la richiesta
 * -2 - l'utente non è presente
 * -3 - connessione al db fallita
 * -4 - bind dei dati fallito
 * -5 - la vecchia password non coincide con quella nuova
 */
function usr_srv_edit_my_password(int $id, string $new_password, string $old_password)
{
    return usr_srv_a_edit_password($id, $new_password, $old_password, true);
}

/**
 * Funzione di modifica password di un utente. Funzione riservata agli amministratori.
 * @param int $id , dell'utente all quale modificare la password
 * @param string $new_password nuova password, ancora da criptare
 * @param string $old_pdw vecchia password se richiesta
 * @param bool $auth_data_old per definire se deve essere verificata la vecchia password. Per il reset
 * da parte degli amministratore.
 * @return int code
 * 1 - password cambiata
 * 0 - password nulla
 * -1 - Connessione non riuscita
 * -2 - l'utente non è presente
 * -4 - Bind dei dati fallito
 * -5 - La password attuale risulta diversa
 */
function usr_srv_a_edit_password(int $id, string $new_password, string $old_pdw, $auth_data_old)
{
    if (empty($new_password)) return 0;
    $new_password = crypt_password($new_password);

    $con = (new Database())->getConnection();

    if ($con!=null) {
        $con->beginTransaction();

        //PARTE DI CONTROLLO PRESENZA UTENTE
        $stmt = $con->prepare("SELECT " . DB_USR_ID . "," . DB_USR_PWD . " FROM user WHERE usr_id = :id");
        if (!$stmt->bindParam(":id", $id)) return -4;

        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);// il risultato ha e deve avere una sola linea
        $conta = $stmt->rowCount();

        //SE L'UTENTE È PRESENTE
        if ($conta > 0) {
            $db_pwd = $row[DB_USR_PWD];
            if ($auth_data_old and password_verify($old_pdw, $db_pwd) == false) {
                //SE NON COMBACIANO
                $con->rollBack();
                return -5;
            } else return usr_srv_edit_password_query($id, $new_password, $con);
        } else {
            $con->rollBack();
            return -2;
        }
    }
    return -1;
}


/**
 * Metodo di per ridurre il codice di a_edit_password
 * @param int $id_change_pwd , l'id dell'utente alla quale cambiare la password
 * @param string $newpwd , la nuova password
 * @param $connection , la connessione al database
 * @return int result
 * -4 == Bind non riuscito
 * -1 - se errore
 *  1 - se la modifica è stata effettuata con successo
 */
function usr_srv_edit_password_query(int $id_change_pwd, string $newpwd, PDO $connection): int
{
    $stmt = $connection->prepare("UPDATE user SET " . DB_USR_PWD . "=:newpwd  WHERE " . DB_USR_ID . "=:id");
    if (!$stmt->bindParam(":newpwd", $newpwd)) return -4;
    if (!$stmt->bindParam(":id", $id_change_pwd)) return -4;
    $executed = $stmt->execute();
    if (!$executed) {
        $connection->rollBack(); //ERRORE ESEGUZIONE
        return -1;
    } else {
        $affected = $stmt->rowCount();
        if ($affected == 1) {
            $connection->commit(); //RIGA MODIFICATA
            return 1;
        } else {
            //SE NON SONO STATE MODIFICATE RIGHE
            $connection->commit();
            return -1;
        }
    }
}

/**
 * Funzione per visualizzare il profilo di un utente
 * @param int, l'id dell'utente della quale si deriserano le informazione
 * @return User con nome cognome e descrizione, se l'id è nullo o c'è stato un errore ritorna null;
 */
function usr_srv_get_profile_visit(int $id): User
{
    $con = (new Database())->getConnection();
    if ($con != null and isset($id)) {
        $stmt = $con->prepare("SELECT usr_id,usr_firstname,usr_lastname,usr_description,usr_auth_lvl FROM user WHERE usr_id=:id");
        if ($stmt->bindParam(":id", $id, PDO::PARAM_INT)) {
            if ($stmt->execute()) {
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                return new User($row);
            }
        }
    }
    return null;
}

/**
 * Cerca gli utenti corrispondenti all'input. La ricerca non restituisce utenti con
 * caratteristiche uguali ma somiglianti ovvero anche con un id, nome, cognome,
 * email non completi.
 * @param string $search_data la stringa su cui viene basata la ricerca, essa può essere formata da
 * più parole separate da spazi ma vengono prese in considerazione solo le prime tre. I termini di
 * ricerca devono essere id, nome, cognome, o email. gli altri non funzionano.
 *
 * @return array contenente gli utenti che corrispondono anche parzialmente ai dati inseriti.
 */
function usr_srv_search_user(string $search_data): array
{
    $data = explode(" ", $search_data);
    $data_num = count($data);
    if ($data_num <= 0) return array();

    $query = "SELECT usr_id,usr_firstname, usr_lastname,usr_description FROM user";
    $query = $query . " WHERE";

    for ($i = 0; $i < $data_num and $i < 3; $i++) {
        $query = $query . " (user.usr_id LIKE :data" . $i . " OR user.usr_firstname LIKE CONCAT('%',:data" . $i . ",'%') OR user.usr_lastname LIKE CONCAT('%',:data" . $i . ",'%'))";
        if ($i != ($data_num - 1)) $query = $query . " AND";
    }
    $query = $query . " LIMIT 10;";


    $con = (new Database())->getConnection();
    if ($con != null) {
        $stmt = $con->prepare($query);
        for ($i = 0; $i < $data_num and $i < 3; $i++) {
            if (!$stmt->bindParam(":data" . $i, $data[$i], PDO::PARAM_STR)) return array();
        }
        if ($stmt->execute()) {
            if ($stmt->rowCount() > 0) { // SE ESEGUITO E CI SONO RISULTATI
                $to_return = array();
                $row = null;
                while (($row = $stmt->fetch(PDO::FETCH_ASSOC)) != null) {
                    array_push($to_return, new User($row));
                }
                return $to_return;
            }
        }
    }
    return array();

}
